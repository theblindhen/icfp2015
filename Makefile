
.PHONY: submission_folder
submission.tar.gz: submission_folder
	cd submission; tar -cvzf ../submission.tar.gz *

submission_folder:
	cp icfp2015.sln submission/
	cp -r packages submission/ 
	-mkdir submission/icfp2015
	cd icfp2015; cp *fs *fsproj packages.config packages.config ../submission/icfp2015/

test_submission: submission_folder
	cd submission; make; ./play_icfp2015 -f ../qualifiers/problem_0.json -f ../qualifiers/problem_1.json -t 22 -m 1000 -p chthulu -p yaggoth
