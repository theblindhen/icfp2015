﻿module Pretty

open Model

let transpose2D (xss: 'a [,]) =
  Array2D.init (Array2D.length2 xss)
               (Array2D.length1 xss)
               (fun x y -> xss.[y,x])

type canvas = char [,] 

let empty_canvas (dimensions : int * int) : canvas =
  let width,height = dimensions
  Array2D.create (2*width + 2) height ' '

let draw_board (canvas : canvas) (board: Board) (dimensions : int * int) =
  let width,height = dimensions
  for x in 0 .. width-1 do
    for y in 0 .. height-1 do
      let c = if is_filled board (x,y) then '●' else '○'
      canvas.[2*x + (y &&& 1) + 1, y] <- c

let print_canvas (canvas : canvas) =
  for y in 0 .. Array2D.length2 canvas - 1 do
    for x in 0 .. Array2D.length1 canvas - 1 do
      System.Console.Write(canvas.[x,y])
    printfn ""

let print_board (board : Board) (dimensions : int * int) =
  let canvas = empty_canvas dimensions
  draw_board canvas board dimensions
  print_canvas canvas
      
let print_unit (unit : Unit) =
  let xs = List.map fst (unit.pivot :: unit.members)
  let ys = List.map snd (unit.pivot :: unit.members)
  let minx, maxx, miny, maxy = List.min xs, List.max xs, List.min ys, List.max ys
  let width, height = (maxx - minx) + 1, (maxy - miny) + 1
  let canvas = empty_canvas (width, height)
  begin
    let x,y = unit.pivot
    canvas.[2*x + (y &&& 1) + 0, y] <- '('
    canvas.[2*x + (y &&& 1) + 2, y] <- ')'
  end
  for (x,y) in unit.members do
    canvas.[2*x + (y &&& 1) + 1, y] <- '★'
  print_canvas canvas

(* TODO: support off-board pivot *)
let print_state (state : State) =
  let canvas = empty_canvas state.dimensions
  draw_board canvas state.board state.dimensions
  begin // mark pivot point
    let x,y = state.unit.pivot
    if x >= 0 && x < fst state.dimensions && y >= 0 && y < snd state.dimensions then
      canvas.[2*x + (y &&& 1) + 0, y] <- '('
      canvas.[2*x + (y &&& 1) + 2, y] <- ')'
  end
  for (x,y) in state.unit.members do
    canvas.[2*x + (y &&& 1) + 1, y] <- '★'

  // Finally, print the canvas
  print_canvas canvas
