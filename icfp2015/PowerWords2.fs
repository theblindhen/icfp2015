﻿module PowerWords2

open Sim
open Util
open Model

let toLower (lst : command list) =
  List.map (fun e -> System.Char.ToLower e) lst

let power_word_cost (power_word : string) =
  let row_count = 
    power_word 
    |> Seq.map (function | SE | SW -> 1 | _ -> 0)
    |> Seq.fold (+) 0
  let length = Seq.length power_word
  float row_count / (float length)

let sort_power_words (power_words : string list) =
  power_words
  |> List.map (fun s -> s.ToLower())
  |> List.sortBy power_word_cost

let found_power_words = [ "ei!"; "r'lyeh"; "yuggoth"; "ia! ia!"; "YogSothoth"; "Tsathoggua"; "Cthulhu fhtagn!"; "John Bigboote" ]

let get_power_words input_power_words =
  match input_power_words with
  | [] -> found_power_words 
  | _  -> input_power_words
  |> sort_power_words

// Try to insert a powerword and get back to previous strategy
let step cmd state = 
  match apply_command state cmd with
  | Moved state -> Some (state)
  | _ -> None

let rec check_rep_errors state cmds =
  if cmds = "" then
    true
  else  
    match cmds.[0] with
    | SE | SW -> true
    | cmd ->
      match apply_command state cmd with
      | Moved state -> check_rep_errors state cmds.[1..]
      | Locked _ -> true
      | RepeatedLocation -> false

let steps state cmds =
  Seq.fold (fun ostate cmd -> Option.bind (step cmd) ostate) (Some(state)) cmds  

// todo: do we want better pathfinding?
let goto_state (state : State) (target_state : State) =
  let find_next_cmd state target_state =
    match state.rot_history.curr_rot, target_state.rot_history.curr_rot with
    | curr_rot, target_rot when curr_rot > target_rot -> Some c_CounterClockwise
    | curr_rot, target_rot when curr_rot < target_rot -> Some c_Clockwise
    | _ ->
      match state.unit.pivot, target_state.unit.pivot with
      | (x, _), (target_x, _) when x > target_x -> Some c_W
      | (x, _), (target_x, _) when x < target_x -> Some c_E
      | (_, y), (_, target_y) when y < target_y -> if y % 2 = 0 then Some c_SE else Some c_SW
      | _ -> None // we can't go up
  let rec loop state target_state rev_cmd =
    if state.rot_history.curr_rot = target_state.rot_history.curr_rot && state.unit.pivot = target_state.unit.pivot then 
      Some(state, rev_cmd |> List.rev |> List.toArray |> (fun s -> System.String(s)))
    else 
      match find_next_cmd state target_state with
      | None -> None
      | Some cmd ->
        match step cmd state with
        | None -> None
        | Some(next_state) -> loop next_state target_state (cmd::rev_cmd)
  loop state target_state []

let try_insert_power state (sub_solution : string) (power_word : string) =
  match steps state power_word with
  | None -> None
  | Some power_state -> 
    seq { 
      yield (state, sub_solution) 
      yield! Seq.unfold (Option.bind (fun (state,sub_solution) ->
        if String.length sub_solution > 0 then                   
          match step sub_solution.[0] state with
          | None -> None
          | Some next_state -> 
            let res = (next_state, sub_solution.[1..])
            Some (res, Some res)
        else
          None)) (Some(state, sub_solution)) 
    }
    |> Seq.truncate 25
    |> Seq.tryPick (fun (target_state, remainder) -> 
                      match goto_state power_state target_state with
                      | None -> None
                      | Some (goto_state, goto_cmd) -> 
                        if check_rep_errors goto_state remainder then
                          Some (power_state, power_word, goto_cmd + remainder)
                        else
                          None)

let try_insert_powers state sub_solution power_words =
  match state with
  | State state ->
      power_words
      |> sort_power_words
      |> Seq.tryPick (try_insert_power state sub_solution)
  | GameEnded _ -> failwith "try_insert_power: GameEnded"

let max_insert_powers power_words (state: State) (solution : seq<string>) =
  let rec loop agg (state: NextState) sub_solution =
    if String.length sub_solution < 2 then
      let state = Seq.fold force_command state sub_solution
      (state, Util.reverse agg + sub_solution)
    else
      match try_insert_powers state sub_solution power_words with
      | Some (pw_state, pw_word, remainder) -> loop (Util.reverse pw_word + agg) (State pw_state) remainder
      | None -> loop (sub_solution.[0..0] + agg) (Sim.force_command state sub_solution.[0]) (sub_solution.[1..])
  Seq.scan (fun (state, _) sub_solution ->
      loop "" state sub_solution
    ) (State state, "") solution
  |> Seq.skip 1
  |> Seq.map (fun (state, sub_solution) ->
      match state with
      | State { score=score } -> (sub_solution, score)
      | GameEnded { score=score } -> (sub_solution, score)
    )

let insert_pwords (power_words : string list) (problem : Problem) (solution : Solution) =
  let power_words = get_power_words power_words
  let init_state = stateFromSeed problem solution.seed
  let solution_seq = seq { yield System.String(solution.solution |> Seq.toArray) }
  Some (max_insert_powers power_words init_state solution_seq
        |> Seq.map fst
        |> Seq.concat
        |> Seq.toList)

let compute_power_scores (power_words : string list) solution =
  let power_words = get_power_words power_words
  let occurances = ref Map.empty

  let incOccurance pword =
    match Map.tryFind pword !occurances with
    | Some i -> occurances := Map.add pword (i + 1) !occurances
    | None -> occurances := Map.add pword 1 !occurances

  let rec compute_power_scores' remainder =
    power_words
    |> List.map (fun word -> word |> List.ofSeq)
    |> List.iter (fun pword -> if isPrefix pword remainder then incOccurance pword)
    match remainder with
    | _::tl -> compute_power_scores' tl
    | _ -> ()

  let score_pword (pword, rep) = 300 + 2 * (List.length pword) * rep

  compute_power_scores' (toLower solution)

  Map.toList !occurances
  |> List.map score_pword 
  |> List.sum

open NUnit.Framework

[<Test>]
let ``Test insertion of ei!`` () =
  printfn "test"
  let problem = Json.parseProblem "../../../qualifiers/problem_0.json"
  let pw = "ei!"
  let sol_str = "aaaaaaaa"
  let solution = { problemId = 0; seed = 0; tag = ""; solution = sol_str |> List.ofSeq; score = 2749 }
  let init_state = stateFromSeed problem solution.seed
  match try_insert_power init_state sol_str pw with
  | Some (pwr_state,pwr,remainder) ->
      Assert.AreEqual(pw + sol_str.[1..], pwr + remainder)
  | None ->
      Assert.Fail("Unable to include power_word")

[<Test>]
let ``Test insertion of phrases of power`` () =
  printfn "test"
  let problem = Json.parseProblem "../../../qualifiers/problem_0.json"
  let sol_str = seq { yield "aaaaaa" }
  let solution = { problemId = 0; seed = 0; tag = ""; solution = sol_str |> Seq.concat |> Seq.toList ; score = 2749 }
  let init_state = stateFromSeed problem solution.seed
  let res_sol = max_insert_powers ["ei!"; "ia!"] init_state sol_str
  Assert.AreEqual("ei!"+"ia! ia!"+"p" |> Seq.toList, res_sol)
