﻿module ServerComm

open Newtonsoft.Json

open System
open System.Text

open FSharp.Data
open FSharp.Data.HttpRequestHeaders

open System.Collections.Generic
open FifteenBelow.Json

// The Curse of the Hens
//let URL = "https://davar.icfpcontest.org/teams/76/solutions"
//let API_KEY = "3xx0oBfAAnGJS0wZan06+IHlfRTg6SM9tP2AGjme87Q="

// The Blind Hen
let URL = "https://davar.icfpcontest.org/teams/67/solutions"
let API_KEY = "xWkW7/z7rOrwkDPF/eziMVFbcS0Tq55xlOtGLAXpFnM="

let submit_solutions solutions =
  Http.RequestString
    ( URL,
      headers = [ ContentType HttpContentTypes.Json;
                  HttpRequestHeaders.BasicAuth "" API_KEY ],
      body = TextRequest (Json.solutions2json solutions))

let converters = [| OptionConverter () :> JsonConverter |]

type SubRecord =
  {
    powerScore : int option;
    seed : int;
    tag : string;
    createdAt : string;
    score : int option;
    authorId : int;
    teamId : int;
    problemId : int;
    solution : string
  }

let input2subrecord (input : string) : SubRecord list =
  JsonConvert.DeserializeObject<SubRecord list>(input, converters = converters)

let read_results () =
  Http.RequestString
    ( URL,
      httpMethod = "GET",
      headers = [ HttpRequestHeaders.BasicAuth "" API_KEY ])
  |> input2subrecord


// Obtain a score from the submission. If it is not listed directly in the sub,
// attempt to obtain it from its tag
let submission_score (sub : SubRecord) : int =
  let parse_tag (tag : String) =
    // find the last (-) in the tag and try to parse the contents as an integer
    try
      Some
        ((tag.Split(['('] |> Array.ofList) |> List.ofArray |> List.rev |> List.head).Split([')'] |> Array.ofList)
         |> List.ofArray |> List.head |> Int32.Parse)
    with
    | _ -> None
  match sub.score, parse_tag sub.tag with
  | Some server_score, Some tag_score -> server_score
  | None, Some tag_score -> tag_score
  | _ -> 0
      


  
open Model
open NUnit.Framework

[<Test>]
let ``Test parsing of json sub records`` () =
  let record_str = "[{ \"powerScore\": null, \"seed\": 4, \"tag\": \"test\", \"createdAt\": \"date\", \"score\": 4, \"authorId\": 1, \"teamId\": 3, \"problemId\": 2, \"solution\": \"abb\" }]"
  let record = List.head (input2subrecord record_str)
  Assert.AreEqual(None, record.powerScore)
  Assert.AreEqual(Some 4, record.score)

(*
[<Test>]
let ``Test solution submissions`` () =
  let sol : Solution = { problemId = 0 ; seed = 0 ; tag = "unit-test" ; solution = [ Move E; Move W ] }
  printfn "%s" (submit_solutions [sol])
*)
