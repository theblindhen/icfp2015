﻿(*module PowerWords

open Sim
open Util
open Model

let toLower (lst : command list) =
  List.map (fun e -> System.Char.ToLower e) lst

let power_words = 
  List.map List.ofSeq [ "ei!"; "yuggoth"; "r'lyeh"; "ia! ia!"; "YogSothoth" ]
  |> List.map toLower

let power_words_rep = 
  [    
    "ei!"; "yuggoth"; "r'lyeh"; "ia! ia!"
  ] 
  |> List.sortBy String.length
  |> List.rev
  |> List.map (fun s -> s, commands_to_string(Seq.map normalize s))
 
let (|Power|) (s:string) =
  power_words_rep
  |> List.filter (fun (_,ps) -> s.StartsWith(ps))
  |> List.map (fun (pw,_) -> pw, s.Substring(String.length pw))

let max_power (solution : string) = 
  let rec loop agg remainder =
    match remainder with
    | Power ((pw, rest) :: _) -> loop (agg + pw) rest    
    | _ -> if remainder.Length < 2
           then agg + remainder 
           else (loop (agg + remainder.[0..0]) remainder.[1..])
  loop "" solution

let step_command state cmd =
  match apply_command state cmd with
  | Moved state -> State state
  | Locked (_, nextState) -> nextState.Force()
  | RepeatedLocation -> failwith ""

let step_commands state cmds =
  try
    List.fold (fun state cmd -> 
      match state with
      | State state -> step_command state cmd
      | GameEnded _ -> failwith "") (State state) cmds |> Some
  with
  | _ -> None

let valid_exec state cmds =
  match step_commands state cmds with
  | Some (GameEnded _) -> true
  | _ -> false

let split n lst =
  let rec split' acc n lst =
    match n, lst with
    | 0, tl -> (acc, tl)
    | _, [] -> [], []
    | _, hd::tl -> split' (hd::acc) (n-1) tl
  let l1, l2 = split' [] n lst
  List.rev l1, l2

let splitAtDown lst =
  let rec split' acc lst =
    match lst with
    | SW as hd::tl -> (hd::acc, tl)
    | SE as hd::tl -> (hd::acc, tl)
    | hd::tl -> split' (hd::acc) tl
    | _ -> (acc, []) 
  let l1, l2 = split' [] lst
  List.rev l1, l2

// assumes simulator doesn't score power phrases
let can_replace_pword currState (remainder : command list) (pword, replaceLen) =

  let replaceTarget, targetRem = split replaceLen remainder

  let tillDown, _ = splitAtDown targetRem

  let targetEndState = step_commands currState (replaceTarget @ tillDown)
  let reducedEndState = step_commands currState (pword @ tillDown)

  match targetEndState, reducedEndState with
  | Some targetEnd, Some redEnd -> targetEnd = redEnd
  | _ -> false


// assumes initial solution causes an end-game condition
let insert_pwords (problem : Problem) (solution : Solution) =

  let initState = stateFromSeed problem solution.seed

  let searchSpace = 
    List.fold (fun acc word -> (List.map (fun rep -> (word, rep)) [0; 1; 2; 3; 4]) @ acc) [] power_words
 
  let rec loop agg currState (remainder : command list) =
    
    match List.tryFind (can_replace_pword currState remainder) searchSpace with
    | Some (pWord, repLength) ->
        let _, rest = split repLength remainder
        match step_commands currState pWord with
        | Some (State state) -> loop ((List.rev pWord) @ agg) state rest
        | Some (GameEnded _) -> (List.rev pWord) @ agg
        | None -> failwith "something is wrong"
    | None ->
        match remainder with
        | [] -> agg
        | currCmd::nextCmds -> 
            match step_command currState currCmd with
            | State nextState -> loop (currCmd::agg) nextState nextCmds
            | GameEnded endState -> 
                if nextCmds = [] then (currCmd::agg)
                else failwith "this should not happen"

  try
    Some (loop [] initState solution.solution |> List.rev)
  with
  | _ -> None

let compute_power_scores solution =

  let occurances = ref Map.empty

  let incOccurance pword =
    match Map.tryFind pword !occurances with
    | Some i -> occurances := Map.add pword (i + 1) !occurances
    | None -> occurances := Map.add pword 1 !occurances

  let rec compute_power_scores' remainder =
    power_words
    |> List.iter (fun pword -> if isPrefix pword remainder then incOccurance pword)
    match remainder with
    | _::tl -> compute_power_scores' tl
    | _ -> ()

  let score_pword (pword, rep) = 300 + 2 * (List.length pword) * rep

  compute_power_scores' (toLower solution)

  Map.toList !occurances
  |> List.map score_pword 
  |> List.sum

open NUnit.Framework

(*
[<Test>]
let ``Test insertion of phrases of power`` () =
  printfn "test"
  let problem = Json.parseProblem "../qualifiers/problem_0.json"
  let solStr = "aaaaaaaampaaaaaammapmmmmmmdmmbbaammamkmkbaammamdmabaaaaaammkbaaaaaakambaaaaaadabmmmbkmkmmbmmmmamabaaamdambaaamdmmbaaaadaaddbmmmmamkmabaaaaaadmbmmmmmbdmbmbmmmmmmkmamdbaaaaaaabmmmmmmbbdmbaaaaaambaaaaamdappmmmmmmdmmbmmmmmmakabaammamkmmmmmmmdmabaaaakampaaaammdbaaaaamdaabmmmmmmbdmmbmmmmammkkbmmmmamamabaaaammamabaaaaaakabaaaaaaaambmmmmmmamabaammamamabaaaammamabmmmmamkmkbmmmmamamabaaaaaammkmbaaaaaadaabaaaaaadabaaaakampmmamamampmmmmmbkmbmmmmmbdmabaaaaaapmmmmamkmbmmmmbbmmbaammamambaaaammaapmmamamampaaaaaambmmmmdmmbaaaadaaabmmamamkmmbaammamambmmmmmmmbmbaammamkmkbaaaaamkmkbaaamkmkmbmmmmmmbkmmbaaaaaammabmmmmmmammbaaamdadambaaaadaaabmmmmkmkmbbmmmmmmambaaamkmkmbmmmmdmmbaaaammkmabmmamamabaaamkakabaaaaaaaapaaaaaammpmmmmmmdmbaammamambmmmmmmmbdmbaaaaaaamkapmmmmmmkmbmmamamammbaaaammamabaammamkmkbaaamkmkmbaaaaamdambaaaaaaabmmmmmmmbkbmmamamambmmmmmmdmbaaaakakabmmmmdmbmmmmamdbaammdmbaaaammabmmamamabaaaakakaaabaaaaaambmmmmmmbmbmmmmamkmbaammdmdmb"
  let solution = { problemId = 0; seed = 0; tag = ""; solution = solStr |> List.ofSeq; score = 2749 }
  match insert_pwords problem solution with
  | Some str ->
      printfn "%s" (new System.String(str |> Array.ofList))
  | None ->
      printfn "something went wrong"
  Assert.AreEqual(true, false)
*)

[<Test>]
let ``Test power score computation`` () =
  Assert.AreEqual(306, compute_power_scores ("mmEi!" |> Seq.toList))

*)