﻿module Sim

open Model

type EndReason = | SourcesDone
                 | FilledBoard of Unit list // remaining sources
                 | Abandoned // AI gave up

type EndState =
  { board : Board
    dimensions : int * int
    score : int
    reason : EndReason
    }

let endStateOfState (state : State) =
  { board = state.board
    dimensions = state.dimensions
    score = state.score
    reason = Abandoned }
  
type NextState =
  | State of State
  | GameEnded of EndState

type Outcome =
  | Moved  of State
  | Locked of Unit * Lazy<NextState>
  | RepeatedLocation

let unit_allowed dimensions board (unit: Unit) =
  List.forall (fun mem ->
      cell_in_board dimensions mem && not (Set.contains mem board)
    ) unit.members

// Lock the unit in the board and clear filled rows
let lock_unit dimensions board unit =
  (* lock unit into the board *)
  let new_board = board_with_locked board unit
  let rows_affected =
    List.map (fun (x,y) -> y) unit.members |> Set.ofList
  let (width, height) = dimensions
  let (cleared, row_destiny) =
    (0, [])
    |> List.foldBack (fun y (cleared, out) ->
          let should_be_cleared =
            Set.contains y rows_affected &&
            List.forall (fun x -> Set.contains (x,y) new_board) [0 .. width-1]
          if should_be_cleared
          then (cleared+1, None :: out)
          else (cleared,   Some cleared :: out)
        ) [0 .. height-1]
    |> (fun (cleared, row_destiny) -> (cleared, Array.ofList row_destiny))
  let cleared_board =
    if cleared = 0 then new_board
    else
      new_board
      |> Set.fold (fun new_board (x,y) ->
            match row_destiny.[y] with
            | None -> new_board
            | Some dy -> Set.add (x, y + dy) new_board
          ) Set.empty
  (cleared_board, cleared)

let apply_command (state: State) (command: command) : Outcome  =
  let new_unit = command_unit command state.unit
  match upd_rot_history state.rot_history new_unit command with
  | None -> RepeatedLocation
  | Some new_hist ->
    if unit_allowed state.dimensions state.board new_unit
    then
      Moved { state with unit = new_unit; rot_history = new_hist }
    else
      let nextState = lazy (
        let (new_board, rows_cleared) = lock_unit state.dimensions state.board state.unit
        let move_score =
          let points = List.length state.unit.members + 100 * (1 + rows_cleared) * rows_cleared / 2
          let line_bonus = if state.ls_old > 1
                           then (state.ls_old - 1) * points / 10
                           else 0
          points + line_bonus
        let new_score = state.score + move_score
        let gameEnded reason =
          GameEnded
            { board = new_board;
              dimensions = state.dimensions;
              score = state.score + move_score;
              reason = reason
              }
        match state.sources with
          | [] -> gameEnded SourcesDone
          | next_unit :: next_sources ->
              if not (unit_allowed state.dimensions new_board next_unit) then
                gameEnded (FilledBoard state.sources)
              else
                State { board = new_board;
                        dimensions = state.dimensions;
                        unit = next_unit;
                        sources = next_sources;
                        ls_old = rows_cleared;
                        score = new_score;
                        rot_history = init_rot_history next_unit 0 }
        )
      Locked (state.unit, nextState)

let force_command (state: NextState) (command: command) : NextState  =
  match state with
  | GameEnded _ -> failwith "force_command run from GameEnded"
  | State state ->
      match apply_command state command with
      | Moved state -> State state
      | RepeatedLocation -> failwith "force_command resulted in RepeatedLocation!"
      | Locked (_, nextState) -> nextState.Force()

open System.IO
open NUnit.Framework

let make_unit unit_members = 
  { 
    Model.members = unit_members; 
    Model.pivot = (0,0) ; 
    Model.maxRot = Model.max_rot_allowed (0,0) unit_members 
  }

let call_lock_unit dimensions board unit_members =
  lock_unit dimensions (Set.ofList board) (make_unit unit_members)

[<Test>]
let ``Test unit_allowed`` () =
  Assert.AreEqual(true, unit_allowed (2,2) (Set.ofList [(0,0)]) (make_unit [(1,1)]))
  Assert.AreEqual(true, unit_allowed (2,2) (Set.ofList [(0,0);(1,0);(0,1)]) (make_unit [(1,1)]))
  Assert.AreEqual(false, unit_allowed (2,2) (Set.ofList [(0,0)]) (make_unit [(0,0)]))

[<Test>]
let ``Test lock_unit 1x1`` () =
  let (state', cleared) = call_lock_unit (1, 1) [] [ (0,0) ]
  Assert.AreEqual(Set.ofList [], state')
  Assert.AreEqual(1, cleared)

[<Test>]
let ``Test lock_unit 2x2`` () =
  Assert.AreEqual((Set.ofList [(0,1)], 1),
                  call_lock_unit (2, 2) [ (0,0) ] [ (0,1);(1,1) ])

[<Test>]
let ``Test lock_unit 2x5`` () =
  Assert.AreEqual((Set.ofList [ (1,2); (0,3); (1,4) ], 2),
                  call_lock_unit (2, 5) [ (1,0); (1,1); (1,3); (1,4) ]
                                        [ (0,1); (0,2); (0,3) ])

[<Test>]
let ``Test scoring`` () =
  let parseProblem file =
    let input = File.ReadAllText file
    Json.input2problem input
  let runCommands state commands =
    List.fold (fun (outs, state) command ->
      let out = apply_command state command
      match out with
      | Moved state -> (out::outs, state)
      | Locked (_,nextState) ->
          (match nextState.Force() with
          | State state -> (out::outs, state)
          | GameEnded _ -> Assert.Fail "The game should not have ended"; (outs, state))
      | RepeatedLocation -> Assert.Fail "RepeatedLocation occurred"; (outs, state)
      ) ([], state) commands

  let problem = parseProblem "../qualifiers/problem_6.json"
  let state = stateFromSeed problem 0

  let (outs, state') = runCommands state (List.init 13 (fun _ -> c_W))
  Assert.AreEqual (8, state'.score)
