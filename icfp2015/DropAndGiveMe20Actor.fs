﻿module DropAndGiveMe20Actor

open System.Threading
open Model
open Sim
open System

let actor(moves) : Actor.actor =
  let get_command (state : State) =
    let test_move cmd =
      match apply_command state cmd with
      | Moved _ -> true 
      | Locked _ -> false
      | RepeatedLocation -> false      
    let rec loop = function
    | move :: moves -> 
      if List.isEmpty moves || test_move move
      then move
      else loop moves
    | [] -> failwith "Missing moves"           
    Some (loop moves)
  {
    get_command = get_command
    get_tag = fun score -> sprintf "%A - DropAndGiveMe20 (%A) (%d)" DateTime.Now (Model.commands_to_string moves) score
  }
