module Actor

open System
open Model

type actor =
  { get_command : State -> command option
    get_tag : int(*score*) -> string
    }

let replay_actor moves more_left =
  more_left := false
  let moves_left = ref moves
  let get_command (state : State) =
    match !moves_left with
      | [] -> None
      | hd::tl -> moves_left := tl; more_left := (tl <> []); Some hd
  {
    get_command = get_command
    get_tag = fun _ -> ""
  }

let human_actor moves =
  printfn "NEW GAME UNDERWAY!"
  let moves_left = ref moves
  let all_moves = ref (List.rev moves)
  let get_command (state : State) =
    let mutable cmd = None
    match !moves_left with
      | [] -> 
          while cmd = None do
            printfn "Enter new command (W: 'w', E: 'e', SW: 's', SE: 'd', RotCW: 'r', RowCCW: 'q', $: print solution)"
            cmd <-
              match Console.ReadKey().KeyChar with
                | 'w' | 'e' | 's' | 'd' | 'r' | 'q' as c -> Some (c)
                | '$' -> 
                    printfn "\nMoves: %s" (commands_to_string (List.rev !all_moves))
                    None
                | _ ->
                    printfn "\ntInvalid input!"
                    None
          let new_move = Option.get cmd
          all_moves := new_move :: !all_moves;
          Some new_move
      | hd::tl -> 
         moves_left := tl; Some hd
  {
    get_command = get_command
    get_tag = fun score -> sprintf "%A - HumanActor (%d)" DateTime.Now score
  }

let go_west_actor =
  {
    get_command = fun _ -> Some c_W
    get_tag = fun score -> sprintf "%A - GoWestActor (%d)" DateTime.Now score
  }
