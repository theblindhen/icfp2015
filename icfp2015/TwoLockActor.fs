module TwoLockActor

open Model
open Sim
open System
open Util

open OneLockActor

open AIHelper

let CONSIDER_PLACEMENTS = 5



let step_command state cmd =
  match apply_command state cmd with
  | Moved state -> State state
  | Locked (_, nextState) -> nextState.Force()
  | RepeatedLocation -> failwith ""

let step_commands state cmds =
  try
    List.fold (fun state cmd -> 
      match state with
      | State state -> step_command state cmd
      | GameEnded _ -> failwith "GameEnded before the end?") (State state) cmds |> Some
  with
  | _ -> None


let actor debug : Actor.actor =
  let moves_left = ref []
  let inner_data = ref Map.empty

  let get_command (state : State) =
    match !moves_left with
    | command :: commands ->
        (* If there's something left to do, do it first *)
        moves_left := commands
        Some command
    | [] ->
        let heuristic = rate_my_state
        let possible_sequences, inner_data' = actor_get_possible_sequences debug state !inner_data
        let consider_for_next = 
          possible_sequences
          |> List.ofSeq
          |> List.map (fun c -> (heuristic c, c))
          |> List.sortBy (fun (h,_) -> -h)
        let consider_for_next =
          match consider_for_next with
          | a1::a2::a3::a4::a5::_ -> a1::a2::a3::a4::a5::[]
          | _ -> consider_for_next
        let _,winner = 
          consider_for_next
          |> Seq.map (fun (my_score,cand : lockedSpot) ->
                        match step_commands state (List.rev cand.commands_rev) with
                        | Some (State state) ->
                            let inner_data' = actor_choose_sequence state inner_data' cand
                            let possible_sequences,_ = actor_get_possible_sequences debug state inner_data'
                            let score = 
                              possible_sequences
                              |> Seq.map heuristic
                              |> Seq.max
                            (my_score + int (1.5 * float score), cand)
                        | Some (GameEnded endState) -> (endState.score - state.score, cand)
                        | None -> (0, cand))
          |> Seq.maxBy fst
        inner_data := actor_choose_sequence state inner_data' winner
        match List.rev winner.commands_rev with
        | [ c ] ->
          moves_left := []
          Some c
        | c :: cmds -> 
          moves_left := cmds
          Some c
        | [] ->
          if debug then
            printfn "ERROR: TwoLockActor had no idea what to do?!?"
          None
  {
    get_command = get_command
    get_tag = fun score -> sprintf "%A - TwoLock (%d)" DateTime.Now score
  }

