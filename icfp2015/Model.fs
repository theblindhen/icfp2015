﻿module Model

open Util

type command = char

type Cell = int * int

type Unit =
  { 
    members : Cell list;
    pivot : Cell;
    maxRot : int;
  }

type Problem =
  { 
    id : int;
    units : Unit array;
    width : int;
    height : int;
    filled : Cell list;
    sourceLength : int;
    sourceSeeds : int list;
  }

type Board = Set<Cell> 

type RotHistory =
  {
    // set of historical and current x-pivots and no# of clockwise rotations (and their symmetries)
    all_rots : Set<int * int>
    curr_rot : int // current no# of clockwise rotations
  }
  
type State =
  { board : Board
    dimensions : int * int // width * height
    unit : Unit
    sources: Unit list // the future sources
    ls_old: int
    score: int
    rot_history : RotHistory
    //TODO: position/rotation history?
    //TODO: command history for power phrases?
  }


type Solution =
  { 
    problemId : int;
    seed : int;
    tag : string;
    solution : command list;
    score : int
  }

let c_E : command = 'b'
let c_W : command = 'p'
let c_SE : command = 'l'
let c_SW : command = 'a'
let c_Clockwise : command = 'd'
let c_CounterClockwise : command = 'k'

let (|E|W|SE|SW|Clockwise|CounterClockwise|) (c:command) =
  match c with
    | 'p' | ''' | '!' | '.' | '0' | '3' -> W
    | 'b' | 'c' | 'e' | 'f' | 'y' | '2' -> E
    | 'a' | 'g' | 'h' | 'i' | 'j' | '4' -> SW
    | 'l' | 'm' | 'n' | 'o' | ' ' | '5' -> SE
    | 'd' | 'q' | 'r' | 'v' | 'z' | '1' -> Clockwise
    | 'k' | 's' | 't' | 'u' | 'w' | 'x' -> CounterClockwise
    | _ -> invalidOp (sprintf "Illegal command in active pattern %c" c)

let normalize = function
  | W -> c_W
  | E -> c_E
  | SW -> c_SW
  | SE -> c_SE
  | Clockwise -> c_Clockwise
  | CounterClockwise -> c_CounterClockwise

let commands_to_string commands =
  System.String(Seq.toArray commands)
  
let cell_in_board (width, height) (x, y) =
  x >= 0 && x < width &&
  y >= 0 && y < height

let rot_cell rot pivot cell =

  let to_grid_cord (x,y) =
    (x - y / 2, y)

  let from_grid_cord (x, y) =
    (x + y / 2, y)

  let rot_grid_cord (dx, dy) =
    match rot with
    | Clockwise -> (-dy, dx + dy)
    | CounterClockwise -> (dx + dy, -dx)
    | _ -> invalidOp (sprintf "Illegal rotation command in rot_grid_cord %c" rot)

  let pivot_grid = to_grid_cord pivot
  
  let add (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)
  let sub (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

  (sub (to_grid_cord cell) pivot_grid) |> rot_grid_cord |> add pivot_grid |> from_grid_cord

let rot_cell_multiple rotcount pivot cell =
  if rotcount = 0 then
    cell
  else
    let dir, count =
      if rotcount < 0 then
        c_CounterClockwise , -rotcount
      else
        c_Clockwise , rotcount
    { 0..count-1 }
    |> Seq.fold (fun cell _ -> rot_cell dir pivot cell) cell

let rot_unit rot unit = 
  { unit with members = List.map (rot_cell rot unit.pivot) unit.members} 

let translate_cell (offx, offy) (x,y) =
  (x + offx, y + offy)

let translate_unit offset unit =
  { unit with 
      members = List.map (translate_cell offset) unit.members;
      pivot = translate_cell offset unit.pivot }

let move_cell dir (x,y) = 
  match dir with
  | E -> ( x + 1, y)
  | W -> ( x - 1, y)
  | SE -> ( x + y % 2, y + 1)
  | SW -> ( x - 1 + y % 2, y + 1)
  | _ -> invalidOp (sprintf "Illegal direction command in move_cell %c" dir)

let move_unit dir unit = 
  { unit with
      members = List.map (move_cell dir) unit.members;
      pivot = move_cell dir unit.pivot }  

let command_unit move unit = 
  match move with
  | E | W | SE | SW -> move_unit move unit
  | Clockwise | CounterClockwise -> rot_unit move unit

let max_rot_allowed pivot members =
  let rec loop max_rot rot_members =
    if max_rot = 5 then max_rot
    else
      let rot_members = List.map (rot_cell c_Clockwise pivot) rot_members
      if List.forall (fun loc -> List.exists ((=) loc) members) rot_members
      then max_rot
      else loop (max_rot+1) rot_members
  loop 0 members

let init_rot_history unit rot =
  {
    all_rots = Set.singleton (fst unit.pivot, modulo rot (unit.maxRot+1))
    curr_rot = rot
  }

let rot_allowed unit new_rot prev_rots =
  not (Set.contains (fst unit.pivot, new_rot) prev_rots)

let upd_rot_history hist unit command =
  match command with
  | SE | SW -> Some (init_rot_history unit hist.curr_rot)
  | _ ->
    let delta_rot =
      match command with
      | Clockwise -> 1
      | CounterClockwise -> -1
      | E | W -> 0
      | _ -> invalidOp (sprintf "Illegal command in update_rot_history %c" command)
    let new_rot = modulo (hist.curr_rot + delta_rot) (unit.maxRot+1)
    if rot_allowed unit new_rot hist.all_rots then
      Some {
        all_rots = Set.add (fst unit.pivot, modulo new_rot (unit.maxRot+1)) hist.all_rots
        curr_rot = new_rot
      }
    else None


// Board functions

let add_filled (board : Board) cell = board.Add (cell)

let is_filled (board : Board) cell = Set.contains cell board 

let initialBoard filled : Board =
  List.fold (fun board cell -> 
      add_filled board cell
    ) Set.empty filled

let initialPosition width (unit : Unit) =
  let xs = List.map fst unit.members
  let minx, maxx = List.min xs , List.max xs
  let deltax = (width - (maxx - minx + 1))/2 - minx
  let deltay = - List.min (List.map snd unit.members)
  translate_unit (deltax,deltay) unit

let get_neighbors (x,y) =
  if y % 2 = 0 
  then [ (x-1,y-1) ; (x,y-1) ; (x-1, y) ; (x+1,y) ; (x-1,y+1) ; (x,y+1) ]
  else [ (x,y-1) ; (x+1,y-1) ; (x-1, y) ; (x+1,y) ; (x,y+1) ; (x+1,y+1) ]

// Return the board that has the given unit locked on it.
// NOTE: This makes no bounds checking for dimensions, and it also doesn't
// verify that all the cells under unit were already empty
let board_with_locked (board : Board) unit =
  List.fold (fun acc mem -> Set.add mem acc) board unit.members



// Problem / State functions
let stateFromSeed (problem : Problem) (seed : int) = 
  let unitIndics =
    Util.lcgen_indices ((uint32) seed) problem.sourceLength (Array.length problem.units)
    |> List.ofSeq
  let units =
    unitIndics
    |> List.map (fun i -> problem.units.[i]) 
    |> List.map (initialPosition problem.width)
  let unit = List.head units
  { 
    board = initialBoard problem.filled
    dimensions = (problem.width, problem.height)
    unit = unit
    sources = List.tail units
    ls_old = 0
    score = 0
    rot_history = init_rot_history unit 0
  } 


// Solutions

let solutionsScore (solutions : Solution list) =
  (List.map (fun (sol : Solution) -> sol.score) solutions |> List.sum)/(List.length solutions)
    


open NUnit.Framework

[<Test>]
let ``test cell rotation around (0,0)`` () =
  let rot_both (x1, y1) (x2, y2) =
    let resx,resy = rot_cell c_Clockwise (0,0) (x1,y1)
    Assert.AreEqual((x2, y2), (resx, resy))
    let resx,resy = rot_cell c_CounterClockwise (0,0) (x2,y2)
    Assert.AreEqual((x1, y1), (resx, resy))
  rot_both (1, -2) (2, 0)
  rot_both (2, 0) (1, 2)
  rot_both (1, 2) (-1, 2)
  rot_both (-1, 2) (-2, 0)


[<Test>]
let ``test cell rotation around (2,3)`` () =
  let rot_both (x1, y1) (x2, y2) =
    let resx,resy = rot_cell c_Clockwise (2,3) (x1,y1)
    Assert.AreEqual((x2, y2), (resx, resy))
    let resx,resy = rot_cell c_CounterClockwise (2,3) (x2,y2)
    Assert.AreEqual((x1, y1), (resx, resy))
  rot_both (3, 5) (1, 5)
  rot_both (-1, 3) (1, 0)
  rot_both (3, 7) (0, 6)
  rot_both (2, 5) (1, 4)
  
[<Test>]
let ``test unit rot clockwise`` () =
  let unit = {
    members= [(1,3);(2,3);(3,3);(3,4)];
    pivot= (0,3);
    maxRot= 5
  }
  let res = rot_unit c_Clockwise unit
  Assert.AreEqual([(1,4);(1,5);(2,6);(1,6)], res.members)
  Assert.AreEqual(unit.pivot, res.pivot)

[<Test>]
let ``test unit rot counter-clockwise`` () =
  let unit = {
    members= [(1,4);(1,5);(2,6);(1,6)];
    pivot= (0,3);
    maxRot= 5
  }
  let res = rot_unit c_CounterClockwise unit
  Assert.AreEqual([(1,3);(2,3);(3,3);(3,4)], res.members)
  Assert.AreEqual(unit.pivot, res.pivot)

[<Test>]
let ``test unit move SW`` () =
  let unit = { members= [(1,3);(2,3);(3,3);(3,4)]; pivot= (0,3); maxRot = 5 }
  let res = move_unit c_SW unit
  Assert.AreEqual([(1,4);(2,4);(3,4);(2,5)], res.members)
  Assert.AreEqual((0,4), res.pivot)

[<Test>]
let ``Test max_rot_allowed`` () =
  let units = [
    { members = [(0,0)]; pivot = (0, 0) ; maxRot = 0 };
    { members = [(0,0); (0, 1)]; pivot = (0, 0) ; maxRot = 5 };
    { members = [(0,1); (2, 0); (2,2)]; pivot = (1, 1) ; maxRot = 1 };
    { members = [(0,1); (1, 1); (2,1)]; pivot = (1, 1) ; maxRot = 2 };
    { members = [(0,1); (1, 1); (2,2)]; pivot = (1, 1) ; maxRot = 5 };
  ]
  for unit in units do
    Assert.AreEqual(unit.maxRot, max_rot_allowed unit.pivot unit.members)

[<Test>]
let ``Test get_neighbors`` () =
  Assert.AreEqual([(0,7); (1,7); (0,8); (2,8); (0,9); (1,9)], get_neighbors (1,8))
  Assert.AreEqual([(2,4); (3,4); (1,5); (3,5); (2,6); (3,6)], get_neighbors (2,5))