﻿module SolutionDB

open System.IO

open Model

open Newtonsoft.Json

type timestamp = int

type dbSolution =
  { 
    problemId : int;
    seed : int;
    tag : string;
    solution : string;
    score : int
    stamp : timestamp
  }

type db = // DB in memory
  { map : Map<int*int, dbSolution> // problemId * seed -> solution
    lastSubmit : timestamp
    timestamp : timestamp
    }

type dbdb = // DB in the file
  { solutions : dbSolution list
    lastSubmit : timestamp
    }
  
let dbifySolution stamp (solution : Solution) : dbSolution = 
  { problemId = solution.problemId 
    seed = solution.seed 
    tag = solution.tag 
    solution = commands_to_string solution.solution
    score = solution.score
    stamp = stamp
  }

let solutionOfDb (dbsolution : dbSolution) : Solution =
  { Solution.problemId = dbsolution.problemId 
    Solution.seed = dbsolution.seed 
    Solution.tag = dbsolution.tag 
    Solution.solution = Seq.toList dbsolution.solution
    Solution.score = dbsolution.score
  }

// let serialize_solution (solution : Solution) =
//   JsonConvert.SerializeObject(dbifySolution solution)

// let deserialize_dbsolution (serialized_solution : string) : dbSolution =
//   JsonConvert.DeserializeObject<dbSolution>(serialized_solution)

// let deserialize_solution (serialized_solution : string) : Solution =
//   solutionOfDb (deserialize_dbsolution serialized_solution)




let dbkey (dbSol : dbSolution) = (dbSol.problemId, dbSol.seed)

let empty_db = { map = Map.empty ; lastSubmit = -1 ; timestamp = 0 }

let dump_db (db : db) filename =
  let solutions = db.map |> Map.toList |> List.map snd
  let dbdb = { solutions = solutions ; lastSubmit = db.lastSubmit } 
  let dbStr = JsonConvert.SerializeObject(dbdb)
  File.WriteAllText(filename, dbStr)

let load_db filename : db =
  try
    let dbdb = JsonConvert.DeserializeObject<dbdb>(File.ReadAllText filename)
    let map : Map<int*int, dbSolution> =
      List.fold (fun map dbsol ->
            map.Add (dbkey dbsol, dbsol)
          ) Map.empty dbdb.solutions
    let timestamp = 1 + (List.max (dbdb.lastSubmit :: (List.map (fun dbsol -> dbsol.stamp) dbdb.solutions)))
    { map = map ; lastSubmit = dbdb.lastSubmit ; timestamp = timestamp}
  with e ->
    printfn "No db file of old solutions!"
    empty_db

let lookup_db (db : db) problemId seed = db.map.[(problemId, seed)]

let update_db (db : db) (dbSolution : dbSolution) debug =
  let key = dbkey dbSolution
  let overwrite = 
    if Map.containsKey key db.map then
      let oldSol = db.map.[key]
      let better = oldSol.score < dbSolution.score
      if debug then
          printfn "New solution for %d, seed %d. Score difference is %i (new-old)" dbSolution.problemId dbSolution.seed (dbSolution.score - oldSol.score)
      better
    else
      if debug then
          printfn "New solution for %d, seed %d. Score is %d (no previous solution)" dbSolution.problemId dbSolution.seed dbSolution.score
      true
  if overwrite
  then ({ db with map = db.map.Add (key, dbSolution) }, true)
  else (db , false)

let updates_db' (db : db) (dbSolutions : dbSolution list) debug skip_pwords =
  List.fold (fun (db, mods) sol ->
          if skip_pwords && sol.tag.Contains(" PW ") then (db, mods)
          else 
            let db, mod'  = update_db db sol debug
            (db, mods || mod')) (db, false) dbSolutions

let updates_db (db : db) (solutions : Solution list) debug =
  updates_db' db (List.map (dbifySolution db.timestamp) solutions) debug

// Update the lastSubmit flag
let submitted_db (db : db) =
  { db with lastSubmit = db.timestamp }
  
// Return all dbSolutions which are newer than lastSubmitted
let since_last_submit (db : db) =
  db.map
  |> Map.toSeq
  |> Seq.filter (fun (_,dbsol) -> dbsol.stamp > db.lastSubmit)
  |> Seq.map (fun (_,dbsol) -> solutionOfDb dbsol)
  |> List.ofSeq

let all_solutions (db : db) =
  db.map
  |> Map.toSeq
  |> Seq.map (fun (_,dbsol) -> solutionOfDb dbsol)
  |> List.ofSeq

// absorb all solutions in second db into the first.
// NOTE: This does not do any checking on stamps
let absorb_db (mother : db) (victim : db) =
  let victimSols =
    victim.map
    |> Map.toList
    |> List.map snd
  updates_db' mother victimSols false false
  |> fst
    
  

let db_solution_of_server_sub (sub : ServerComm.SubRecord) : dbSolution =
  { problemId = sub.problemId
    seed = sub.seed
    tag = sub.tag
    solution = sub.solution
    score = ServerComm.submission_score sub
    stamp = -10
    }

let db_of_server skip_pwords (subs : ServerComm.SubRecord list) =
  updates_db' empty_db (List.map db_solution_of_server_sub subs) false skip_pwords
  |> fst


let print_db_stats (db : db) =
  printfn "TODO: DB stats"



    
