module AIHelper

open Model

type CellSet = Set<Cell>

// Return the set of all on-board cells that neighbours a single cell
let cell_neighbours dimensions (x,y) =
  [ (x-1, y) ; (x,y-1) ; (x+1,y-1) ; (x+1,y) ; (x+1,y+1) ; (x,y+1) ]
  |> List.filter (cell_in_board dimensions)
  
// Return the set of all cells that are neighbouring to a unit
let unit_neighbours dimensions (unit : Unit) =
  List.map (fun cell -> cell_neighbours dimensions cell |> Set.ofList) unit.members
  |> Set.unionMany
  |> (fun allCells -> Set.difference allCells (Set.ofList unit.members))

let hex_distance dx dy =
  abs dy + max 0 (abs dx - (if dy * dx >= 0 then (abs dy) / 2 else (abs dy + 1) / 2))

let unit_radius_floor unit =
  let (x2, y2) = unit.pivot
  List.max (List.map (fun (x1, y1) -> hex_distance (x1-x2) (y1-y2)) unit.members)

// Return all cells that have the same board state as the input cell. Only state.board is considered here, not state.unit
let similar_component dimensions (board : Board) cell =
  let filled = is_filled board cell
  let worklist = ref ([cell])
  let rec lookNext (accSet : CellSet) =
    match !worklist with
      | cell :: worklist' ->
          let accSet =
            if (filled = is_filled board cell) && not (Set.contains cell accSet) then
              worklist := (cell_neighbours dimensions cell) @ worklist'
              accSet.Add (cell)
            else
              worklist := worklist'
              accSet
          lookNext accSet
      | [] -> accSet
  lookNext Set.empty
    

    
// Return the new bubbles created by the move. These are returned as a list of CellSets
let bubbles (state : State) : CellSet list =
  let board = board_with_locked state.board state.unit
  let allBubbles = 
    Seq.fold (fun bubbles cell ->
        if is_filled board cell || (List.exists (Set.contains cell) bubbles) then
          bubbles
        else
          (similar_component state.dimensions board cell) :: bubbles
      ) [] (unit_neighbours state.dimensions state.unit |> Set.toSeq)
  // Remove the bubble with the spawn cell
  let spawn = ((fst state.dimensions)/2, 0)
  List.filter (not << Set.contains spawn) allBubbles
    

let is_simple_bubble dimensions (board : Board) cell =
  not (is_filled board cell)
  && (List.forall (fun n -> is_filled board n) (cell_neighbours dimensions cell))
    
    
// Return the new *immediate neighbour* bubbles created by the move. These are returned as a list of cells
let bubbles_simple (state : State) : Cell list =
  let board = board_with_locked state.board state.unit
  let neighbours = unit_neighbours state.dimensions state.unit
  neighbours
  |> Set.toSeq
  |> Seq.filter (fun cell ->
                 // Keep only unfilled cells
      not (is_filled board cell)
      && (List.forall
                 // Which are surrounded only by filled cells or other unfilled neighbours
                 // Note that on the top row, this can declare some false simple bubbles
            (fun n -> is_filled board n || Set.contains n neighbours)
            (cell_neighbours state.dimensions cell))
    )
  |> List.ofSeq
               

  


open NUnit.Framework

[<Test>]
let ``test cell_neighbours`` () =
  let cell = (1,1)
  printfn "%A" (cell_neighbours (3,2) cell)
  Assert.AreEqual (cell_neighbours (3,2) cell |> List.sort, [ (0,1) ; (1,0) ; (2,0) ; (2,1) ; ])

[<Test>]
let ``test unit_neighbours`` () =
  let unit = { members = [(1,1) ; (0,1)] ; pivot = (0,0) ; maxRot = 5 }
  printfn "%A" (unit_neighbours (3,2) unit |> Set.toList)
  Assert.AreEqual (unit_neighbours (3,2) unit |> Set.toList , [ (0,0) ; (1,0) ; (2,0) ; (2,1) ])

[<Test>]
let ``test hex_distance`` () =
  let cases = [
    0, [0,0];
    1, [1,0; 0,1];
    2, [2,0; 1,1; 1,2; 0,2];
    3, [3,0; 2,1; 2,2; 1,3; 0,3];
    ]
  for expected, dists in cases do
    for dx, dy in dists do
      Assert.AreEqual(expected, hex_distance dx dy)
      Seq.unfold (fun cell -> Some (cell, rot_cell c_Clockwise (0,0) cell)) (dx,dy)
      |> Seq.take 6
      |> Seq.iteri (fun idx (dx',dy') ->
          Assert.AreEqual(expected, hex_distance dx' dy',
            sprintf "rot^%i(%d,%d) = (%d,%d)" idx dx dy dx' dy')
        )
  ()

[<Test>]
let ``test unit_radius`` () =
  let cases = [
    0, { pivot = (2,-1); members = [(2,-1)]; maxRot = 0 }
    2, { pivot = (1,0); members = [(2,1)]; maxRot = 5 }
    2, { pivot = (1,0); members = [(2,2); (2,1)]; maxRot = 5 }
  ]
  for expected, unit in cases do
    Assert.AreEqual(expected, unit_radius_floor unit)
    Seq.unfold (fun unit -> Some (unit, rot_unit c_Clockwise unit)) unit
    |> Seq.take 6
    |> Seq.iteri (fun idx unit' ->
        Assert.AreEqual(expected, unit_radius_floor unit',
          sprintf "rot^%i(%A) = (%A)" idx unit unit')
      )

[<Test>]
let ``test similar_component`` () =
  let board = initialBoard [ (1,0) ; (1,1) ; (0,1) ]
  let dimensions = 3, 2
  Assert.AreEqual(similar_component dimensions board (0,0) |> Set.toList , [ (0,0) ])
  Assert.AreEqual(similar_component dimensions board (1,0) |> Set.toList , [ (0,1) ; (1,0) ; (1,1) ])
  Assert.AreEqual(similar_component dimensions board (1,1) |> Set.toList , [ (0,1) ; (1,0) ; (1,1) ])
  Assert.AreEqual(similar_component dimensions board (0,1) |> Set.toList , [ (0,1) ; (1,0) ; (1,1) ])
  Assert.AreEqual(similar_component dimensions board (2,0) |> Set.toList , [ (2,0) ; (2,1) ])
  Assert.AreEqual(similar_component dimensions board (2,1) |> Set.toList , [ (2,0) ; (2,1) ])

[<Test>]
let ``test bubbles`` () =
  let empty_state = 
    { board = initialBoard [ ]
      dimensions = 3, 3
      unit = { members = [] ; pivot = (0,0) ; maxRot = 0 }
      sources = [] ; ls_old = 0 ; score = 0 ; rot_history = { all_rots = Set.empty ; curr_rot = 0 } }

  // No bubbles test
  let state = { empty_state with board = initialBoard [ (1,1) ]
                                 unit = { members = [ (1,2) ; (0,2) ] ; pivot = (0,0) ; maxRot = 5 } }
  Assert.AreEqual(bubbles state , [])

  // Some bubbles
  
  let state = { empty_state with board = initialBoard [ (0,1) ]
                                 unit = { members = [ (1,1) ; (1,2) ; (2,1) ] ; pivot = (0,0) ; maxRot = 5} }
  let myBubbles = bubbles state |> List.map Set.toList
  Assert.AreEqual(2, List.length myBubbles)
  if myBubbles.[0] = [ (0,2) ] then
    Assert.AreEqual(myBubbles.[1], [ (2,2) ] )
  else
    Assert.AreEqual(myBubbles.[0], [ (2,2) ] )
    Assert.AreEqual(myBubbles.[1], [ (0,2) ] )

[<Test>]
let ``test bubbles_simple`` () =
  let empty_state = 
    { board = initialBoard [ ]
      dimensions = 3, 3
      unit = { members = [] ; pivot = (0,0) ; maxRot = 0}
      sources = [] ; ls_old = 0 ; score = 0 ; rot_history = { all_rots = Set.empty ; curr_rot = 0 } }

  let state = { empty_state with board = initialBoard [ (0,2) ]
                                 dimensions = 3,4
                                 unit = { members = [ (1,2) ; (1,3) ; (2,2) ] ; pivot = (0,0) ; maxRot = 5 } }
  let myBubbles = bubbles_simple state |> List.sort
  Assert.AreEqual(myBubbles, [ (0,3) ; (2,3) ] )
