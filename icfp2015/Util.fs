﻿module Util

let inline cartesian l1 l2 =
  Seq.collect (fun e1 -> Seq.map (fun e2 -> (e1,e2)) l2) l1
  
// Take a ('a * 'b) list and return a ('a * 'b list) list
let groupByFst list =
    list 
    |> List.fold (fun group (g, x) -> 
        match group |> Map.tryFind g with
        | Some s -> group |> Map.add g (x::s)
        | None -> group |> Map.add g [x]
        ) Map.empty
    |> Map.toList 

// Return a map from list elements to its multiplicity in the list
let list_multiplicities list =
  List.fold (fun map e ->
             match Map.tryFind e map with
             | None -> Map.add e 1 map
             | Some m -> Map.add e (m+1) map
             ) Map.empty list

let rec filter_map f = function
    | [] -> []
    | e::ls ->
        match f e with
        | None    -> filter_map f ls
        | Some e' -> e'::filter_map f ls

let split n lst =
  let rec split' acc n lst =
    match n, lst with
    | 0, tl -> (acc, tl)
    | _, [] -> [], []
    | _, hd::tl -> split' (hd::acc) (n-1) tl
  let l1, l2 = split' [] n lst
  List.rev l1, l2

let reverse string = string |> Array.ofSeq |> Array.rev |> (fun s -> System.String(s))

let prand (seed : uint32) multiplier increment =
  multiplier * seed + increment
 
let lcgen seed length =
  let multiplier = 1103515245u
  let increment = 12345u
  Seq.unfold
    (fun seed ->
      let x = prand seed multiplier increment
      Some ((seed &&& 0b01111111111111110000000000000000u) >>> 16, x)
    )
    seed
  |> Seq.take length

let lcgen_indices seed length (range : int) =
  let range = (uint32) range
  lcgen seed length
  |> Seq.map (fun x -> (int) (x % range))

// Compute n mod m, returning the representative in 0,..,m-1
let modulo n m = ((n % m) + m) % m

// checks whether lst1 is a prefix of lst2
let rec isPrefix lst1 lst2 =
  match lst1, lst2 with
  | [], _ -> true
  | hd1::tl1, hd2::tl2 -> hd1 = hd2 && isPrefix tl1 tl2
  | _ -> false

open NUnit.Framework

[<Test>]
let ``test lcgen`` () =
  let expected = [0u; 24107u; 16552u; 12125u; 9427u; 13152u; 21440u; 3383u; 6873u; 16117u]
  Assert.AreEqual(lcgen 17u 10 |> Seq.toList, expected)

[<Test>]
let ``test lcgen_indices`` () =
  let expected = [0; 7; 2; 5; 7; 2; 0; 3; 3; 7]
  Assert.AreEqual(lcgen_indices 17u 10 10 |> Seq.toList, expected)
