﻿module Json

open Model

open System.IO
open Newtonsoft.Json

type jsonSolution =
  { 
    problemId : int;
    seed : int;
    tag : string;
    solution : string;
  }

let jsonifySolution (solution : Solution) : jsonSolution = 
  { 
    problemId = solution.problemId; 
    seed = solution.seed; 
    tag = solution.tag; 
    solution = commands_to_string solution.solution; 
  }

let solution2json (solution : Solution) =
  JsonConvert.SerializeObject(jsonifySolution solution)

let solutions2json (solutions : Solution list) =
  let jsols = List.map jsonifySolution solutions
  JsonConvert.SerializeObject(jsols)

type jsonCell =
  { 
    x: int;
    y: int
  }

type jsonUnit =
  { 
    members : jsonCell list;
    pivot : jsonCell;
  }

type jsonProblem =
  { 
    id : int;
    units : jsonUnit array;
    width : int;
    height : int;
    filled : jsonCell list;
    sourceLength : int;
    sourceSeeds : int list;
  }


let input2problem (input : string) : Problem =
  // Note: This does not give an error if stuff is wrong
  let jproblem = JsonConvert.DeserializeObject<jsonProblem>(input)
  if box jproblem.units = null || box jproblem.filled = null then
    invalidOp (sprintf "Parsed problem was not sane:\n\t%s" input)
  let jcell2cell (jcell : jsonCell) = (jcell.x, jcell.y)
  let junit2unit (junit : jsonUnit) =
    let members = List.map jcell2cell junit.members
    let pivot = jcell2cell junit.pivot
    {
      Model.members = members
      Model.pivot = pivot
      Model.maxRot = Model.max_rot_allowed pivot members
    }
  { 
    Model.id = jproblem.id
    Model.units = Array.map junit2unit jproblem.units
    Model.width = jproblem.width
    Model.height = jproblem.height
    Model.filled = List.map jcell2cell jproblem.filled
    Model.sourceLength = jproblem.sourceLength
    Model.sourceSeeds = jproblem.sourceSeeds
  }

let parseProblem file = 
  let input = File.ReadAllText file
  input2problem input