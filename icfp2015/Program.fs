﻿module Program

open System
open System.Text
open System.IO

open FSharp.Collections.ParallelSeq

open Model
open Json
open Actor

let DB_FILE     = "solutionCache.dat"
let SERVERDB_FILE = "serverSubmissions.dat"
let MERGEDB_FILE = "mergeSolutions.dat"

let qualifiers = 
  [
    "../qualifiers/problem_0.json";
    "../qualifiers/problem_1.json";
    "../qualifiers/problem_2.json";
    "../qualifiers/problem_3.json";
    "../qualifiers/problem_4.json";
    "../qualifiers/problem_5.json";
    "../qualifiers/problem_6.json";
    "../qualifiers/problem_7.json";
    "../qualifiers/problem_8.json";
    "../qualifiers/problem_9.json";

    "../qualifiers/problem_10.json";
    "../qualifiers/problem_11.json";
    "../qualifiers/problem_12.json";
    "../qualifiers/problem_13.json";
    "../qualifiers/problem_14.json";
    "../qualifiers/problem_15.json";
    "../qualifiers/problem_16.json";

    "../qualifiers/problem_17.json";
    "../qualifiers/problem_18.json";
    "../qualifiers/problem_19.json";
    "../qualifiers/problem_20.json";
    "../qualifiers/problem_21.json";
    "../qualifiers/problem_22.json";
    "../qualifiers/problem_23.json";
    "../qualifiers/problem_24.json";
]

  

type invocation =
  { files : string list
    timeLimit : int option
    memoryLimit : int option
    phrases : string list
    cores : int option
    other : string list

    debug : bool
    debugHeavy : bool
    summary : bool
    serial : bool
    insertPWords : bool
    skipDownload : bool
    skipPWords : bool
  }

let rec parseArgs = function
  | "-f" :: file :: argl ->
      let inv = parseArgs argl
      { inv with files = file :: inv.files }
  | "-t" :: limit :: argl ->
      let inv = parseArgs argl
      { inv with timeLimit = Some (Int32.Parse limit) }
  | "-m" :: limit :: argl ->
      let inv = parseArgs argl
      { inv with memoryLimit = Some (Int32.Parse limit) }
  | "-p" :: phrase :: argl ->
      let inv = parseArgs argl
      { inv with phrases = phrase :: inv.phrases }
  | "-c" :: cores :: argl ->
      let inv = parseArgs argl
      { inv with cores = Some (Int32.Parse cores) }

  // Our custom defined flags and stuff
  | "-D" :: argl ->
      { parseArgs argl with debug = true }
  | "-DD" :: argl ->
      { parseArgs argl with debugHeavy = true ; debug = true }
  | "-S" :: argl ->
      { parseArgs argl with serial = true }
  | "-NOPW" :: argl ->
      { parseArgs argl with insertPWords = false }
  | "-SKIPPW" :: argl ->
      { parseArgs argl with skipPWords = true }
  | "-SKIPDL" :: argl ->
      { parseArgs argl with skipDownload = true }
  | "-H" :: argl ->
      { parseArgs argl with summary = true }
  | o :: argl ->
      let inv = parseArgs argl
      { inv with other = o :: inv.other }
  | [] ->
      { files = []
        timeLimit = None
        memoryLimit = None
        cores = None
        phrases = []

        serial = false
        debug = false
        debugHeavy = false
        insertPWords = true
        skipPWords = false
        summary = false
        skipDownload = false
        
        other = [] }



let printProblemStats (problem: Problem) =
  printfn "Problem %d stats" problem.id
  printfn "Dimensions: %d x %d" problem.width problem.height
  printfn "No. of units: %d" (Array.length problem.units)
  printfn "%% of filled cells: %f" (100.0 * ((float) (List.length problem.filled)) / ((float) (problem.width * problem.height)))
  printfn "No. of seeds: %d" (List.length problem.sourceSeeds)
  printfn "Source length: %d" problem.sourceLength
  let board = Model.initialBoard problem.filled
  Pretty.print_board board (problem.width, problem.height)
  List.iteri (fun i unit -> printfn "Unit %d" i; Pretty.print_unit unit) (problem.units |> List.ofArray)
  printfn ""


let solveSeed actor problem debug seed =
    let state = Model.stateFromSeed problem seed
    let rec cmdMore accCmds (state : State) =
      if debug then
        printfn "\nCurrent state (Score %d):" state.score
        Pretty.print_state state
      match actor.get_command state with
      | Some cmd ->
          match Sim.apply_command state cmd with
            | Sim.Moved state -> cmdMore (cmd::accCmds) state
            | Sim.Locked (_, nextState) ->
                match nextState.Force() with
                | Sim.State state ->  cmdMore (cmd::accCmds) state
                | Sim.GameEnded endState -> cmd::accCmds, endState
            | Sim.RepeatedLocation -> invalidOp "GameLoop: the AI made the Sim threw RepeatedLocation!"
      | None -> accCmds, Sim.endStateOfState state
    let cmdsRev, endState = cmdMore [] state
    ({Solution.problemId = problem.id
      Solution.seed = seed
      Solution.tag = actor.get_tag endState.score
      Solution.solution = List.rev cmdsRev
      Solution.score = endState.score
       },
     endState)

let gameLoop (args : invocation) (actor : actor) (problem : Problem) =
  if args.debug then
    printfn "gameLoop (problemId %d, seeds: %A)" problem.id problem.sourceSeeds
  if args.serial then
    problem.sourceSeeds |> Seq.map (solveSeed actor problem args.debugHeavy) |> Seq.toList
  else
    problem.sourceSeeds |> PSeq.map (solveSeed actor problem args.debugHeavy) |> PSeq.toList

let printSolutionSummaries (solutions : Solution list) =
  List.iter (fun (sol : Solution) ->
      printfn "Solution for problemId %d, seed %d:\t%d" sol.problemId sol.seed sol.score
    ) solutions

let printSolutionAndStateSummaries (solutionAndStates : (Solution * Sim.EndState) list) =
  List.iter (fun (sol : Solution, endState : Sim.EndState) ->
      let reason = match endState.reason with
                   | Sim.SourcesDone -> "SourcesDone"
                   | Sim.FilledBoard rest -> sprintf "FilledBoard %d" (List.length rest)
                   | Sim.Abandoned -> "Abandoned!!!"
      printfn "Solution for problemId %d, seed %d:\t%d\tEnded by %s" sol.problemId sol.seed sol.score reason
    ) solutionAndStates

let printSolutions solutions =
  printfn "%s" (Json.solutions2json solutions)

let scoreSolution args problem seed cmds =
  let more_left = ref false
  let actor = (replay_actor cmds more_left)
  try 
    let sol,_ = solveSeed actor problem false seed
    if more_left.Value then 0 else sol.score + PowerWords2.compute_power_scores args.phrases cmds
  with
  | :? System.InvalidOperationException -> 0


let optPowerWords args problem solution =
  match PowerWords2.insert_pwords args.phrases problem solution with
  | Some optSolution ->
      let newScore = scoreSolution args problem solution.seed optSolution
      if newScore > solution.score then
        if args.debug then
          printfn "%d, %d: score %d -> %d" solution.problemId solution.seed solution.score newScore
        Some { problemId = solution.problemId; seed = solution.seed; 
               tag = sprintf "%s PW (%d)" solution.tag newScore;
               solution = optSolution; score = newScore }
      else None
  | None -> None

let runActors args actors problems  =
  let db = ref (SolutionDB.load_db DB_FILE)
  let actorProblems = Seq.collect (fun a -> List.map (fun p -> (a,p)) problems) actors
  let lockObj = new obj()
  let iter = if args.serial then Seq.iter else PSeq.iter
  let optSol problem sol = match optPowerWords args problem sol with Some opt -> opt | None -> sol
  let synchronized f = lock lockObj f
  printfn "PW %A" args.insertPWords
  actorProblems
  |> iter (fun (actor, problem) ->
        let solutionsAndStates = gameLoop args actor problem 
        if args.summary then
           printSolutionAndStateSummaries solutionsAndStates
        let solutions =
           solutionsAndStates
           |> List.map fst
           |> List.map (if args.insertPWords then optSol problem else id)
        if args.summary && args.insertPWords then
           printfn "After PWords:"
           printSolutionSummaries solutions
        synchronized (fun () ->
            let db', mods = SolutionDB.updates_db !db solutions args.debug args.skipPWords
            if mods then
              db := db'
            SolutionDB.dump_db !db DB_FILE
          )
      )

let compute_high_scores subs =
  let parse_tag (tag : String) =
    // find the last (-) in the tag and try to parse the contents as an integer
    try
      Some
        ((tag.Split(['('] |> Array.ofList) |> List.ofArray |> List.rev |> List.head).Split([')'] |> Array.ofList)
         |> List.ofArray |> List.head |> Int32.Parse)
    with
    | _ -> None

  List.fold (fun acc (sub : ServerComm.SubRecord) ->
    let score =
      match sub.score, parse_tag sub.tag with
      | Some server_score, Some tag_score -> server_score
      | None, Some tag_score -> tag_score
      | _ -> 0
    match Map.tryFind (sub.problemId, sub.seed) acc with
    | None ->
        Map.add (sub.problemId, sub.seed) score acc
    | Some high ->
        if score > high then
          Map.add (sub.problemId, sub.seed) score acc
        else
          acc) Map.empty subs

let dump_server args =
  if args.skipDownload then
    SolutionDB.load_db SERVERDB_FILE
  else
    let serverdb = ServerComm.read_results () |> SolutionDB.db_of_server args.skipPWords
    SolutionDB.dump_db serverdb SERVERDB_FILE
    printfn "Server database dumped to %s" SERVERDB_FILE
    serverdb

let localRun argl = 
  let args = parseArgs argl

  if System.Environment.OSVersion.Platform < System.PlatformID.Unix then
    Console.OutputEncoding <- Encoding.Unicode

  let problems = 
    (if args.files = [] then qualifiers else args.files)
    |> List.map parseProblem

  match args.other with
  | [ "stats" ] ->
      problems |> List.iter printProblemStats

  | [ "dropandgiveme20" ] ->
      printfn "DropAndGiveMe20 playing the game"
      let actor = DropAndGiveMe20Actor.actor([c_SW; c_SE; c_E])
      runActors args [actor] problems

  | [ "onelockactor" ] -> 
      printfn "onelock, solving %A" args.files
      runActors args [OneLockActor.actor args.debug] problems

  | [ "twolockactor" ] -> 
      printfn "twolock, solving %A" args.files
      runActors args [TwoLockActor.actor args.debug] problems

  | "interactive" :: seed :: tl ->
      printfn "Doing the game"
      let problem = { List.head problems with sourceSeeds = [ Int32.Parse seed ] }
      let moves = 
        match tl with
        | [ str ] -> Seq.toList str
        | _ -> []
      gameLoop args (Actor.human_actor moves) problem
      |> List.map fst
      |> printSolutions

  | [ "high_scores" ] ->
      printfn "Computing high scores."
      printfn " id    seed      score"
      let submissions = ServerComm.read_results ()
      let high_scores = compute_high_scores submissions
      high_scores |> Map.iter (fun (probId, seed) score ->
        printfn "  %-3d   %-6d   %5d" probId seed score)

  | [ "check_scores" ] ->
      printfn "Checking our scores against those computed by server."
      let submissions = ServerComm.read_results ()
      let problems = List.map parseProblem qualifiers
      List.iter 
        (fun (sub : ServerComm.SubRecord) ->
          match sub.score, sub.powerScore with
          | Some score, Some pscore ->
              let problem = problems.[sub.problemId]
              let ourScore = scoreSolution args problem sub.seed (sub.solution |> Seq.toList)
              if ourScore <> score then
                printfn "- problem: %d, seed: %d, our score: %d, server score: %d, server powerScore: %d" sub.problemId sub.seed ourScore score pscore
                printfn "- solution: %s" sub.solution
          | _ -> ()
          ) submissions

  | "mass_submit" :: problem_id :: seed :: prefix :: file :: tag :: _ ->

     let to_sol str : Model.Solution = { 
        problemId = Int32.Parse problem_id;
        seed = Int32.Parse seed;
        tag = str;
        solution = str |> Seq.toList;
        score = 0 
     }

     let input = File.ReadAllLines file
     input 
     |> List.ofArray 
     |> List.map (fun word -> prefix + word)
     |> List.map to_sol 
     |> List.iter (fun sol -> 
          printfn "submitting %s" sol.tag
          try
            ServerComm.submit_solutions [{ sol with tag = tag }] |> ignore
          with
          | _ -> printfn "failed %s" sol.tag)

  | "submit_direct" :: problem_id :: seed :: file :: _ ->

     let str = File.ReadAllText file
     let solution : Model.Solution = { 
        problemId = Int32.Parse problem_id;
        seed = Int32.Parse seed;
        tag = "";
        solution = str |> Seq.toList;
        score = 0 }
     ServerComm.submit_solutions [solution] |> ignore

  | "insert_powerwords" :: _ ->
     
    let db = ref (SolutionDB.load_db DB_FILE)
    let serverdb = dump_server args
    serverdb.map 
    |> Map.iter (fun (id, seed) sol ->
      let problem = Json.parseProblem (sprintf "../qualifiers/problem_%d.json" id)
      let solution = { 
        problemId = id; 
        seed = seed; 
        tag = ""; 
        solution = sol.solution |> List.ofSeq; 
        score = sol.score }
      match optPowerWords args problem solution with
      | Some optSolution ->
          printfn "%d,%d: scores %d -> %d" id seed sol.score optSolution.score
          match SolutionDB.updates_db !db [optSolution] args.debug false with
          | db', true -> db := db' | _ -> ()
          SolutionDB.dump_db !db DB_FILE
      | None -> ())

  | [ "server" ] ->

    dump_server args |> ignore

  | "submit" :: tl ->
    printfn "WHAT SAY YOU! ALL YOUR BASE ARE BELONG TO jabber.ru"
    let db = SolutionDB.load_db DB_FILE
    let serverdb = ServerComm.read_results () |> SolutionDB.db_of_server false
   // let serverdb = SolutionDB.load_db SERVERDB_FILE
    let mergeddb =
      let absorbed = SolutionDB.absorb_db serverdb db
      { absorbed with lastSubmit = db.lastSubmit ; timestamp = db.timestamp }
    let betterSolutions = SolutionDB.since_last_submit mergeddb
    if betterSolutions = [] then
      printfn "No new solutions to submit :-("
      SolutionDB.dump_db mergeddb MERGEDB_FILE
      printfn "Merged database dumped to %s" MERGEDB_FILE
    else
      printfn "New solutions for the following (id * seed):"
      List.iter (fun (sol : Solution) ->
                 let oldScore =
                   let oldSol = SolutionDB.lookup_db serverdb sol.problemId sol.seed
                   oldSol.score
                 printfn "\t%2d, %6d (new score: %6d, old score: %6d" sol.problemId sol.seed sol.score oldScore
              ) betterSolutions
      match tl with
      | [ "-upload" ] ->
          printfn "Uploading"
          ServerComm.submit_solutions betterSolutions |> ignore
          SolutionDB.dump_db (SolutionDB.submitted_db db) DB_FILE
          SolutionDB.dump_db (SolutionDB.submitted_db mergeddb) MERGEDB_FILE
      | _ ->
          SolutionDB.dump_db mergeddb MERGEDB_FILE
          printfn "NOTE: No solutions were uploaded! (use -upload)"
      printfn "Merged database dumped to %s" MERGEDB_FILE


//  | ["maxpower"] ->
//    printfn "Powerizing solutions"
//    let db = SolutionDB.load_db ()
//    db.map
//    |> Map.toSeq
//    |> Seq.map (fun (_,dbsol) -> (dbsol.solution, PowerWords.max_power dbsol.solution))
//    |> Seq.filter (fun (old_sol, new_sol) -> old_sol <> new_sol)
//    |> Seq.iter (fun (old_sol, new_sol) -> printfn "converted %s to %s" old_sol new_sol)

  | _ ->
    printfn "Didn't understand arguments: %A" args.other
    printfn "Usage: read Program.fs"

  0 // return an integer exit code

open System.Threading

type SyncObject(cores, maxJobs) =
  let mutable expected = maxJobs
  let mutable running = 0
  let queue = System.Collections.Generic.Queue<unit -> unit>()
  let bestSolutions = System.Collections.Concurrent.ConcurrentDictionary()

  member self.BestSolutions = bestSolutions

  member self.QueueTask(f: unit -> unit) =
    let wrap_f() =
      f ()
      let next_job =
        lock self (fun () ->
            expected <- expected - 1
            if expected = 0 then
              Monitor.Pulse(self)
            try Some (queue.Dequeue())
            with :? System.InvalidOperationException ->
              running <- running - 1
              None
          )
      match next_job with
      | None -> ()
      | Some f -> f ()
    lock self (fun () ->
        if running < cores then
          running <- running + 1
          let thread = System.Threading.Thread(wrap_f)
          thread.IsBackground <- true
          thread.Start()
        else
          queue.Enqueue(wrap_f)
      )

  member self.Wait() =
    lock self (fun () ->
        while expected > 0 do
          Monitor.Wait(self) |> ignore
      )

  member self.WaitMS(milliseconds: int) =
    lock self (fun () ->
        (* Assumes no spurious wakeups *)
        if expected > 0 then
          Monitor.Wait(self, milliseconds) |> ignore
      )

let playIncremental actor state =
  let rec cmdMore accCmds (state : State) =
    match actor.get_command state with
    | Some cmd ->
        match Sim.apply_command state cmd with
          | Sim.Moved state -> cmdMore (cmd::accCmds) state
          | Sim.Locked (_, nextState) ->
              match nextState.Force() with
              | Sim.State state ->
                  seq {
                    yield (commands_to_string (List.rev (cmd::accCmds)), state.score)
                    yield! cmdMore [] state
                  }
              | Sim.GameEnded endState ->
                  seq { yield (commands_to_string (List.rev (cmd::accCmds)), state.score) }
          | Sim.RepeatedLocation -> invalidOp "GameLoop: the AI made the Sim threw RepeatedLocation!"
    | None ->
        seq { yield (commands_to_string (List.rev accCmds), state.score) }
  cmdMore [] state

let playAndInsertPowerWords (sync: SyncObject) power_words (actor, problem: Problem, seed) =
  let yield_solution score command_string =
    let solution =
      { Solution.problemId = problem.id
        Solution.seed = seed
        Solution.tag = actor.get_tag score
        Solution.solution = Seq.toList command_string
        Solution.score = score }
    sync.BestSolutions.AddOrUpdate((problem.id, seed), solution,
      fun _ old_solution ->
        if solution.score > old_solution.score then solution else old_solution)
      |> ignore
  let power_words = PowerWords2.get_power_words power_words
  let state = Model.stateFromSeed problem seed
  playIncremental actor state
  //|> Seq.map (fun (chunk, score) ->
  //    printfn "before score %d, chunk %s" score chunk
  //    (chunk, score)
  //  )
  (* TODO: scoring it slightly different with or without powerword insertion,
   * even if it doesn't actually insert anything. I think it's because the
   * powerword version scores for the last move. *)
  |> Seq.map fst
  |> PowerWords2.max_insert_powers power_words state
  //|> Seq.map (fun (chunk, score) ->
  //    printfn "after  score %d, chunk %s" score chunk
  //    (chunk, score)
  //  )
  |> Seq.fold (fun prefix (chunk, score) ->
        let commands = prefix + chunk
        let power_score = PowerWords2.compute_power_scores power_words (Seq.toList commands)
        yield_solution (score + power_score) commands
        commands
      ) ""
  |> ignore

//yield_solution endState.score (cmd::accCmds)
//                  yield_solution state.score (cmd::accCmds)

let releaseRun argl =
  let args = parseArgs argl
  let cores = match args.cores with None -> System.Environment.ProcessorCount | Some cores -> cores
  let problems = List.map parseProblem args.files
  let actors = [
      DropAndGiveMe20Actor.actor([c_SW; c_SE; c_E])
      OneLockActor.actor false
      TwoLockActor.actor false
    ]
  let jobs =
    seq {
      for actor in actors do
        for problem in problems do
          for seed in problem.sourceSeeds do
            yield (actor, problem, seed)
    }
    |> List.ofSeq
  let sync = SyncObject(cores, List.length jobs)
  for job in jobs do
    sync.QueueTask(fun () ->
        //let (_, _, seed) = job
        //printfn "Starting thread %A" seed
        try
          playAndInsertPowerWords sync args.phrases job
        with _ -> () (* swallow exception in release mode *)
        //printfn "Ending thread %A" seed
      )
  match args.timeLimit with
  | None -> sync.Wait()
  | Some seconds -> let seconds_for_printing = 2
                    sync.WaitMS(1000 * max 1 (seconds - seconds_for_printing))
  let results = sync.BestSolutions.ToArray()
  printfn "["
  Array.iteri (fun i (KeyValue (_, result)) ->
      printf "  %s" (Json.solution2json result)
      if i <> Array.length results - 1
      then printfn ","
      else printfn ""
    ) results
  printfn "]"
  System.Environment.Exit(0) (* I've seen it hang. Make sure it doesn't. *)
  0



[<EntryPoint>]
let main argv = 
  if argv.Length = 0 || argv.[0] = "release" then
    releaseRun (List.tail (List.ofArray argv))
  else 
    localRun (List.ofArray argv)
