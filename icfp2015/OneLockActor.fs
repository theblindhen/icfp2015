﻿module OneLockActor

open Model
open Sim
open System
open Util

open AIHelper


let FAR_DOWN_YMULT = 200
let FAR_DOWN_XMULT = 1

let BUBBLE_PENALTY = 100

let CLEARED_ROWS_BONUS = 10000



type placement = {
  x: int
  y: int
  rotation: int (* 0 through 5 *)
}

type candidate = {
  commands_rev: command list
  state: State
}

type lockedSpot = {
  commands_rev : command list
  lastState : State
  nextState : Lazy<Sim.NextState>
}

type exploration = 
  | Candidate of candidate
  | LockedSpot of lockedSpot

type heuristic = lockedSpot -> int

let heuristic_far_down ({ lastState = { unit={ members=members }}} : lockedSpot) = 
  let score_cell (x,y) = y * FAR_DOWN_YMULT + x * FAR_DOWN_XMULT
  List.sum (List.map score_cell members)

  
// let heuristic_bubble_penalty ({ lastState = lastState } : lockedSpot) =
//   //TODO: Optimise that bubbles_simple doesn't recompute board lock
//   let bubbles = AIHelper.bubbles_simple locked
//   -BUBBLE_PENALTY * List.length bubbles

let heuristic_clear_rows { lastState = { dimensions=dimensions; board=board; unit=unit } } =
  let _, rows_cleared = Sim.lock_unit dimensions board unit
  rows_cleared * rows_cleared * CLEARED_ROWS_BONUS


(* Put the new unit in a place with many neighbouring obstacles, where some
 * neighbours are better than others. As a tie breaker, also try to move it far
 * down. *)
let heuristic_neighbours
  { lastState = { unit = {members=members}; dimensions=(width, height); board=board }
    commands_rev=commands_rev } =
    List.sumBy (fun (x,y) ->
        y +
        List.sumBy (fun (border_x,border_y) -> 
          if border_x < 0 || border_x >= width || border_y>=height || Set.contains (border_x,border_y) board 
          then border_y - y + 2 
          else 0) (get_neighbors (x,y))
    ) members

let heuristic_sum (heuristics : heuristic list) : heuristic =
  fun state ->
    List.sumBy (fun h -> h state) heuristics

// let rate_my_state = heuristic_neighbours
let rate_my_state =
  heuristic_sum [ heuristic_neighbours ; heuristic_clear_rows ]
// let rate_my_state = heuristic_sum [ heuristic_far_down ; heuristic_clear_rows ; heuristic_bubble_penalty ]
  
// Return placement arising after a command
let inline placement_after (candidate : candidate) cmd =
  let unit = candidate.state.unit
  let (x,y), rotation = unit.pivot, candidate.state.rot_history.curr_rot
  match cmd with
  | E | W | SE | SW ->
    let x,y = move_cell cmd (x,y)
    { x=x; y=y;   rotation=rotation }
  | CounterClockwise -> { x=x; y=y;   rotation=modulo (rotation+5) (unit.maxRot+1) }
  | Clockwise        -> { x=x; y=y;   rotation=modulo (rotation+1) (unit.maxRot+1) }

let unit_commands (unit : Unit) : command list =
  let maxRot = unit.maxRot
  [ c_E; c_W; c_SE; c_SW ]
  @ if maxRot = 0 then []
    else if maxRot = 1 then [ c_Clockwise ]
    else [ c_Clockwise; c_CounterClockwise ]

let inline explore (candidate : candidate) (command : command) =
  let cmds = command :: candidate.commands_rev
  match Sim.apply_command candidate.state command with
  | Moved state -> 
      Some (Candidate { commands_rev = cmds
                        state = state })
  | Locked (_,cnext) ->
      Some (LockedSpot { commands_rev = cmds
                         lastState = candidate.state
                         nextState = cnext })
  | RepeatedLocation ->
      None

// Find explorations for this candidate
// Don't call explore on already visited placements
// The exception is when the placement would lock the unit and we didn't already lock it from here
let explorations_for_candidate visited lockPlacements candidate cmds =
  let saw_locked = ref false
  // printfn "Locks: %A" (Set.toList lockPlacements)
  let expl, visited, lockPlacements =
    cmds
    |> List.fold (fun (expl, visited, lockPlacements) cmd ->
        let placementAfter = placement_after candidate cmd
                  //TODO: Check for placementAfter is on the board
        if Set.contains placementAfter visited then 
          if !saw_locked || not (Set.contains placementAfter lockPlacements) then
            // This is a visited placement. If it is in lockPlacements, we
            // want to visit it only if we didn't already saw a Locked for this cand.
            (expl, visited, lockPlacements)
          else
            saw_locked := true
            (match explore candidate cmd with
             | Some (LockedSpot ls) -> LockedSpot ls :: expl
             | Some (Candidate c) ->
                 // printfn "cmd: %A" cmd
                 // printfn "rotation after: %A" c.state.rot_history.curr_rot
                 // printfn "pivot: %A" c.state.unit.pivot
                 // Pretty.print_state c.state
                 // printfn "cmds_rev: %A" c.commands_rev
                 // printfn "locked: %A" (Set.toList lockPlacements)
                 // printfn "placement after: %A" placementAfter
                 failwith "Candidate Monkey!"// This must be a LockedSpot
             | None ->
                 failwith "Monkey!"// This must be a LockedSpot
             , visited
             , lockPlacements) 
        else
          let visited = Set.add placementAfter visited
          match explore candidate cmd with
            | None -> (expl, visited, lockPlacements)
            | Some (Candidate c)   -> (Candidate c::expl, visited, lockPlacements)
            | Some (LockedSpot ls) ->
              // printfn "ADDING LOCKED %d,%d,%d" placementAfter.x  placementAfter.y placementAfter.rotation
              // printfn "%A" ls.lastState.unit.pivot
              // printfn "AT BOARD WITH COMMAND %A" cmd
              // Pretty.print_state ls.lastState
              let lockPlacements = Set.add placementAfter lockPlacements
              ( if !saw_locked then
                  expl
                else
                  saw_locked := true
                  LockedSpot ls::expl
              , visited
              , lockPlacements)
        ) ([], visited, lockPlacements)
  (expl, visited, lockPlacements)

let inline placement_of_candidate (candidate : candidate) =
  let x,y = candidate.state.unit.pivot
  { x=x; y=y; rotation = candidate.state.rot_history.curr_rot }

// The candidates_list below should be sorted such that candidates_list.[i]
// contains only candidates whose command length is i
// The candidates must all be from the same unit
let find_locked_spots_from_candidates (state : State) (candidates_list : candidate list list) =
  let initPlacements =
    match candidates_list with
      | [] -> Set.empty
      | hd::_ -> hd
                 |> List.map placement_of_candidate
                 |> Set.ofList
  let all_cmds = unit_commands state.unit
  let lockedSpot_seqs =
    (candidates_list, initPlacements, Set.empty)
    |> Seq.unfold (fun (candidates_list, visited, lockPlacements) ->
          match candidates_list with
          | [] -> None
          | candidates::candidates_list ->
              let (expll, visited, lockPlacements) =
                candidates
                |> List.fold (fun (expll, visited, lockPlacements) candidate ->
                    let (expl, visited, lockPlacements) =
                              explorations_for_candidate visited lockPlacements candidate all_cmds
                    (expl::expll, visited, lockPlacements)
                  ) ([], visited, lockPlacements)
              let expl = List.concat expll
              let (candidates, lockeds) =
                expl
                |> List.fold (fun (candidates, lockeds) -> function
                      | Candidate candidate -> (candidate :: candidates, lockeds)
                      | LockedSpot locked   -> (candidates, locked :: lockeds)
                  ) ([], [])
              //printfn "Candidates: %d were locked, %d were unlocked" (List.length locked) (List.length unlocked)
              let visited = Set.union visited (candidates |> List.map placement_of_candidate |> Set.ofList)
              let candidates_list =
                match candidates_list with
                  | [] ->
                    if candidates = [] then [] else [ candidates ]
                  | candidates':: candidates_list ->
                    (candidates' @ candidates) :: candidates_list
              Some (lockeds, (candidates_list,visited,lockPlacements))
      )
  Seq.concat lockedSpot_seqs
  

// Simple flood fill from initial state
let find_possible_sequences (state : State) =
  find_locked_spots_from_candidates state [[{ commands_rev = []; state = state }]]



////////////////////////////////////////////////
// FLOOD FILL DATA STRUCTURE
////////////////////////////////////////////////

type unit_data = {
  pruned_commands_rev: (int * Set<command list>) option // invalidated from * pruned cmds
  by_y: Map<int, lockedSpot list>
  radius: int
}

type units_data = Map<Unit, unit_data>

let total_locked_positions (by_y : Map<int, lockedSpot list>) =
  by_y
  |> Map.fold (fun sum _ spots -> sum + List.length spots) 0

let get_unit_data units_data unit =
  match Map.tryFind unit units_data with
  | Some unit_data -> unit_data
  | None -> { pruned_commands_rev = Some(0, Set.singleton [])
              by_y = Map.empty
              radius = unit_radius_floor unit }

let unlocked_state_after_commands state =
  List.fold (fun ostate command ->
      match ostate with
      | None -> None
      | Some state ->
          match Sim.apply_command state command with
          | Sim.Moved state -> Some state
          | _ -> None
    ) (Some state)

open FSharpx.Collections

let build_candidates_worklist (state : State) (unit_data : unit_data) : candidate list list =
  let len_candidate_lists = 
    match unit_data.pruned_commands_rev with
    | None -> []
    | Some (_, pruned) ->
        pruned
        |> Set.toList
        |> filter_map (fun commands_rev ->
              match unlocked_state_after_commands state (List.rev commands_rev) with
              | Some state' -> 
                  let candidate = {
                      commands_rev = commands_rev
                      state = state'
                    }
                  let length = List.length commands_rev
                  Some (length, candidate)
              | None -> None
            )
        |> groupByFst
  let len_candidate_lists =
    if len_candidate_lists = []
    then [(0,[{ commands_rev=[] ; state=state }])] // Always regrow from zero
    else len_candidate_lists
  let maxLen = List.max (List.map fst len_candidate_lists)
  let arr = Array.create (maxLen+1) []
  len_candidate_lists
    |> List.iter (fun (len, candidates) ->
          arr.[len] <- List.ofSeq candidates
        )
  Array.toList arr

let update_by_y (unit_data : unit_data) lockedSpots =
  lockedSpots
  |> Seq.map (fun ls -> (snd ls.lastState.unit.pivot, ls))
  |> List.ofSeq
  |> groupByFst
  |> List.fold (fun by_y (y, spots) ->
        Map.add y spots by_y
      ) unit_data.by_y

let repair_unit_data (state: State) unit_data : unit_data =
  (* Turn pruned_commands_rev into states and add them to the "by_length" priority queue *)
  // printfn "Repairing from\n%A" unit_data.pruned_commands_rev
  let candidates_worklist = build_candidates_worklist state unit_data
  // printfn "Repairing based on %d candidates" (List.sum (List.map List.length candidates_worklist))
  // printfn "%A" candidates_worklist
  let lockedSpots = find_locked_spots_from_candidates (state : State) candidates_worklist
  let new_by_y = update_by_y unit_data lockedSpots
  // printfn "\tRegrew %d new lockedSpots" (total_locked_positions new_by_y - total_locked_positions unit_data.by_y)
  // if not (Map.isEmpty unit_data.by_y) then
    // printfn "Max locked spots y: %d" (Map.keys unit_data.by_y |> Seq.max)
  { by_y = new_by_y
    pruned_commands_rev = None
    radius = unit_data.radius }

// For each lockedSpot, cut its command_rev so that it 
// moves only less than y_bound down
let prune_commands y_bound (lockeds : lockedSpot list) =
  if y_bound = 0 then Set.empty
  else
    let rec prune acc cury = function
      | [] ->
        //TODO: Fail-safe?
        failwith "prune_commands should be called only on lockedSpots that reach y_bound?"
      | cmd :: cmds ->
          let nexty =
            match cmd with
              | SE | SW -> cury+1
              | _ -> cury
          if nexty >= y_bound
          then acc
          else prune (cmd::acc) nexty cmds
    lockeds
    |> List.map (fun ls ->
                 try 
                  prune [] 0 (List.rev ls.commands_rev)
                 with exc ->
                   printfn "FAILED at %A" y_bound
                   printfn "ls: %A" ls
                   //TODO: Fail-safe?
                   failwith "MONKEY"
                 )
    |> Set.ofList

let invalidate_units_data (state: State) (winner : lockedSpot) units_data : units_data =
  let invalidate_unit_data unit (unit_data : unit_data) =
    let _,start_y = unit.pivot
    let min_placed_y = winner.lastState.unit.members
                       |> List.map snd
                       |> List.min
    let invalidate_y = max 0 (min_placed_y - unit_data.radius)
    let do_prune () =
      // printfn "Pruning at %A, (unit with count %d). Has y-values at %A" invalidate_y (List.length unit.members) (Map.keys unit_data.by_y |> List.ofSeq)
      // printfn "With unit %A" unit
      if invalidate_y = 0 then
        Map.empty, Set.singleton []
      else
        let pruned =
          unit_data.by_y
          |> Map.fold (fun pruned y lockeds ->
              if y < invalidate_y
              then pruned
              else
                prune_commands (invalidate_y - start_y) lockeds
                |> Set.union pruned
                  // lockeds
                  // |> List.iter (fun ls ->
                  //        let ls.last
                  //        printfn "%A" simulate_commands (List.rev (List.tail ls.commands_rev))
                  //     )
            ) Set.empty
        // if not (Map.isEmpty unit_data.by_y) then
          // printfn "Invalidating %A rows" (Map.keys unit_data.by_y |> Seq.filter (fun y -> y >= invalidate_y) |> Seq.length)
        let new_by_y = Map.filter (fun y _ -> y < invalidate_y) unit_data.by_y
        // printfn "Pruned out %d lockedSpots. New total %d" (total_locked_positions unit_data.by_y - total_locked_positions new_by_y) (total_locked_positions new_by_y)
        new_by_y, pruned
    let by_y, pruned =
        match unit_data.pruned_commands_rev with
        | None ->
            let by_y, pruned = do_prune ()
            (by_y, Some (invalidate_y, pruned))
        | Some (prev_invalidate, prev_pruned) as prev ->
            if invalidate_y >= prev_invalidate then
              (unit_data.by_y, prev)
            else
              let by_y, pruned = do_prune ()
              (by_y, Some (invalidate_y, Set.union pruned prev_pruned))
    { unit_data with pruned_commands_rev = pruned
                     by_y = by_y }
  Map.map (fun unit unit_data -> invalidate_unit_data unit unit_data) units_data

// Debug function
let units_data_verify_ycoords (units_data : units_data) =
  units_data
  |> Map.iter (fun unit udata ->
                udata.by_y
                |> Map.iter (fun y lspots ->
                      lspots |>
                      List.iter (fun ls ->
                                  if ls.lastState.unit.pivot |> snd <> y then
                                    failwith "NOOOOOOOOOOOOOOOOO"
                                  )
                    )
                )

let units_data_verify_pruned_commands (state : State) (units_data : units_data) =
  units_data
  |> Map.iter (fun unit udata ->
               match udata.pruned_commands_rev with
               | None -> ()
               | Some (y_bound, pruned) ->
                  pruned
                  |> Set.toList
                  |> List.iter (fun cmds_rev ->
                          match unlocked_state_after_commands state (List.rev cmds_rev) with
                          | None -> ()
                          | Some state' -> 
                              if state'.unit.pivot |> snd >= y_bound then
                                failwith "NOOOOO"
                        ))
  
let actor_get_possible_sequences debug (state : State) (units_data : units_data) =
  let current_unit = state.unit // This should be in initial pos
  if debug then
    printfn "Don't know what to do with. Figure it out."
    Pretty.print_state state
  let unit_data = get_unit_data units_data current_unit
  let unit_data = repair_unit_data state unit_data
  let possible_sequences = Map.values unit_data.by_y |> Seq.concat
  if Seq.isEmpty possible_sequences then
    // This should never happen! But don't crash if it does
    if debug then
      printfn "ERROR IN FLOOD FILL: No possible move?!"
    let possible_sequences = find_possible_sequences (state : State)
    let unit_data = { unit_data with
                        by_y = possible_sequences
                              |> update_by_y unit_data
                        pruned_commands_rev = None }
    possible_sequences, Map.add current_unit unit_data units_data
  else
    possible_sequences, Map.add current_unit unit_data units_data

let actor_choose_sequence (state : State) units_data winner =
  let current_unit = state.unit // This should be in initial pos
  // Throw away flood fill map if we cleared a line
  match winner.nextState.Force () with
    | GameEnded _ -> Map.empty
    | State nextState ->
        if nextState.ls_old >= 1 then
          Map.empty
        else
          // printfn "Carrying over"
          units_data
          |> invalidate_units_data state winner
      

let actor debug : Actor.actor =
  let moves_left = ref []
  let units_data = ref Map.empty

  let get_command (state : State) =
    match !moves_left with
    | command :: commands ->
        (* If there's something left to do, do it first *)
        moves_left := commands
        Some command
    | [] ->
        let (possible_sequences, units_data') = actor_get_possible_sequences debug state !units_data
        if Seq.isEmpty possible_sequences then
          failwith "No possible sequences"
        let winner = Seq.maxBy rate_my_state possible_sequences
        units_data := actor_choose_sequence state units_data' winner
        let commands = List.rev winner.commands_rev
        match commands with
        | [ c ] ->
          moves_left := []
          Some c
        | c :: cmds -> 
          moves_left := cmds
          Some c
        | [] ->
          if debug then
            printfn "ERROR: OneLockActor had no idea what to do?!?"
          None
  {
    get_command = get_command
    get_tag = fun score -> sprintf "%A - OneLock (%d)" DateTime.Now score
  }




open NUnit.Framework

let placements_of_explorations explorations =
  explorations
  |> List.map (function
               | Candidate c ->   (false, (c.state.unit.pivot, c.state.rot_history.curr_rot))
               | LockedSpot ls -> (true,  (ls.lastState.unit.pivot, ls.lastState.rot_history.curr_rot)))
  |> List.sort

let placements_of_lockedSpots lockedSpots =
  lockedSpots
  |> List.ofSeq
  |> List.map (fun ({ lastState=lastState } : lockedSpot) ->
                (lastState.unit.pivot, lastState.rot_history.curr_rot))
  |> List.sort

[<Test>]
let ``test_explorations_for_candidate`` () =
  let state = {
    board = initialBoard []
    dimensions = 3, 3
    unit = { members = [(1,0)] ; pivot = (1,0) ; maxRot = 0}
    sources = []
    ls_old = 0
    score = 0
    rot_history = { all_rots = Set.empty ; curr_rot = 0 }
    } 
  let candidate = { commands_rev = []
                    state = state}
  let expl,_,_ = explorations_for_candidate Set.empty Set.empty candidate (unit_commands state.unit)
  let placements = placements_of_explorations expl
  printfn "%A" placements
  Assert.AreEqual (placements, 
                   [(false, ((0,0),0))
                    (false, ((0,1),0))
                    (false, ((1,1),0))
                    (false, ((2,0),0))
                    ])

  let state = { state with 
                  board = initialBoard [(2,2)]
                  unit = { members = [(1,2)] ; pivot = (1,2) ; maxRot = 0}
                  } 
  let visited =
    Set.empty
    |> Set.add { x=0; y=2; rotation=0 } 
    |> Set.add { x=2; y=2; rotation=0 } 
  let lockPlacements =
    Set.empty
    |> Set.add { x=2; y=2; rotation=0 } 
  let candidate = { commands_rev = []
                    state = state }
  let expl,visited,lockPlacements =
    explorations_for_candidate visited Set.empty candidate (unit_commands state.unit)
  let placements = placements_of_explorations expl
  printfn "%A" placements
  Assert.AreEqual (placements, 
                   [(true, ((1,2),0))])
      
      

[<Test>]
let ``test_find_possible_sequences`` () =
  let state = {
    board = initialBoard []
    dimensions = 3, 3
    unit = { members = [(1,0)] ; pivot = (1,0) ; maxRot = 0}
    sources = []
    ls_old = 0
    score = 0
    rot_history = { all_rots = Set.empty ; curr_rot = 0 }
    } 
  let placements =
    find_possible_sequences state
    |> placements_of_lockedSpots
  // printfn "%A" placements
  Assert.AreEqual (placements, 
                   [((0,0),0)
                    ((0,1),0)
                    ((0,2),0)
                    ((1,2),0)
                    ((2,0),0)
                    ((2,1),0)
                    ((2,2),0)
                    ])
      

// [<Test>]
// let ``test_find_possible_sequences_rot`` () =
//   let dim = 5
//   let state = {
//     board = initialBoard []
//     dimensions = dim, dim
//     unit = { members = [(0,0)]; pivot = (1,0) ; maxRot = 5}
//     sources = []
//     ls_old = 0
//     score = 0
//     rot_history = { all_rots = Set.empty ; curr_rot = 0 }
//     } 
//   let placements =
//     find_possible_sequences state
//     |> placements_of_lockedSpots
//   let border =
//       [ for i in 0..dim-1 -> (0,i) ]
//     @ [ for i in 0..dim-1 -> (dim-1,i) ] 
//     @ [ for i in 1..dim-2 -> (i,dim-1) ]
//   let expectedPlacements =
//     border
//     |> List.map (fun (x,y) ->
//                   [ for r in 0..5 -> (rot_cell_multiple r (x,y) (x+1,y), r) ])
//     |> List.concat
//   let expectedPlacements |> Set.ofList
//   let expectedPlacements = expectedPlacements |> Set.ofList
//   printfn "%A" placements
//   printfn "%A" (list_multiplicities placements |> Map.toList)
//   let placementSet = placements |> Set.ofList
//   Assert.AreEqual (List.length placements, placementSet |> Set.count)
//   printfn "pl \ exp: %A" (Set.difference placementSet expectedPlacements)
//   printfn "exp \ pl : %A" (Set.difference expectedPlacements placementSet)
//   Assert.AreEqual (Set.difference placementSet expectedPlacements, Set.empty)
//   Assert.AreEqual (Set.difference expectedPlacements placementSet, Set.empty)
//   // Assert.AreEqual (placements, 
//   //                  [((2,0),0);((1,2),1);((-1,2),2);((-2,0),3);((-1,-2),4);((2,-2),5)
//   //                   ((2,1),0);((1,3),1);((-1,3),2);((-2,1),3);((-1,-1),4);((2,-1),5)
//   //                   ((3,1),0);((2,3),1);((0,3),2);((-1,1),3);((0,-1),4);((3,-1),5)
//   //                   ((3,0),0);((2,2),1);((0,2),2);((-1,0),3);((0,-2),4);((3,-2),5)
//   //                   ] |> List.sort)
